package urkaz.alphasin.core.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import urkaz.alphasin.core.blocks.G55Blocks;
import urkaz.alphasin.core.util.Name;

public class G55CreativeTab {
    public static CreativeTabs g55tab;

    public static void init() {
        g55tab = new CreativeTabs(Name.creativeTab) {
            public ItemStack getIconItemStack() {
                return new ItemStack(G55Blocks.craftingPanel, 1, 0);
            }

            @Override
            public Item getTabIconItem() {
                return null;
            }
        };
    }
}
