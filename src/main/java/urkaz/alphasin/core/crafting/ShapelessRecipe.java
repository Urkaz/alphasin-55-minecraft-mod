package urkaz.alphasin.core.crafting;

import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ShapelessRecipe implements IG55Recipe {

    public final List<ItemStack> recipeItems;
    private final ItemStack recipeOutput;

    public ShapelessRecipe(ItemStack output, List<ItemStack> input) {
        this.recipeOutput = output;
        this.recipeItems = input;
    }

    @Override
    public boolean matches(G55CraftingRecipe recipe) {
        ArrayList<ItemStack> arrayList = new ArrayList<ItemStack>(this.recipeItems);

        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                for (int k = 0; k < 2; ++k) {
                    ItemStack itemstack = recipe.getItemInSlot(i + j * 2 + k * 4);

                    if (itemstack != null) {
                        boolean flag = false;

                        for (ItemStack itemStack : arrayList) {
                            if (itemstack.getItem() == itemStack.getItem() && itemstack.getItemDamage() == itemStack.getItemDamage()) {
                                flag = true;
                                arrayList.remove(itemStack);
                                break;
                            }
                        }

                        if (!flag)
                            return false;
                    }
                }
            }
        }
        return arrayList.isEmpty();
    }

    @Override
    public ItemStack getCraftingResult(G55CraftingRecipe recipe) {
        return this.recipeOutput.copy();
    }

    @Override
    public int getRecipeSize() {
        return this.recipeItems.size();
    }

    @Override
    public ItemStack getRecipeOutput() {
        return this.recipeOutput;
    }
}
