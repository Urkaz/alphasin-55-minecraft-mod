package urkaz.alphasin.core.crafting;

import net.minecraft.item.ItemStack;

public class ShapedRecipe implements IG55Recipe {

    private final int recipeWidth;
    private final int recipeDepth;
    private final int recipeHeight;
    private final ItemStack[] recipeItems;
    private final ItemStack recipeOutput;

    public ShapedRecipe(int recipeWidth, int recipeHeight, int recipeDepth, ItemStack[] recipeItems, ItemStack recipeOutput) {
        this.recipeWidth = recipeWidth;
        this.recipeHeight = recipeHeight;
        this.recipeItems = recipeItems;
        this.recipeOutput = recipeOutput;
        this.recipeDepth = recipeDepth;
    }

    @Override
    public boolean matches(G55CraftingRecipe recipe) {
        for (int i = 0; i <= 2 - this.recipeWidth; ++i) {
            for (int j = 0; j <= 2 - this.recipeDepth; ++j) {
                for (int k = 0; k <= 2 - this.recipeHeight; ++k) {
                    for (int r = 0; r < 4; r++) {
                        if (this.checkMatch(recipe, i, j, k, r * 90, false))
                            return true;
                        if (this.checkMatch(recipe, i, j, k, r * 90, true))
                            return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkMatch(G55CraftingRecipe recipe, int ic, int jc, int kc, int rotation, boolean checkSymmetrical) {
        for (int k = 0; k < 2; ++k) { //Height
            for (int j = 0; j < 2; ++j) { //Depth
                for (int i = 0; i < 2; ++i) { //Width

                    int i1 = i - ic;
                    int j1 = j - jc;
                    int k1 = k - kc;
                    ItemStack itemstack = null;

                    int index = -1;
                    int hack = 0;
                    int sum = this.recipeHeight + this.recipeWidth + this.recipeDepth;
                    if (i1 >= 0 && j1 >= 0 && k1 >= 0 && i1 < this.recipeWidth && j1 < this.recipeDepth && k1 < this.recipeHeight) {
                        if (checkSymmetrical) {
                            if (sum == 4)
                                hack = 1;
                            index = this.recipeWidth - i1 - 1 + j1 * this.recipeWidth + k1 * (sum - 2 - sum % 2 - hack);
                            itemstack = this.recipeItems[index];
                        } else {
                            if (sum == 4)
                                hack = 1;
                            index = i1 + j1 * this.recipeWidth + k1 * (sum - 2 - sum % 2 - hack);
                            itemstack = this.recipeItems[index];
                        }
                    }

                    int pos = i + j * 2 + k * 4;
                    ItemStack itemstack1 = recipe.getItemInSlot(pos, rotation);

                    if (itemstack1 != null || itemstack != null) {
                        if (itemstack1 == null && itemstack != null || itemstack1 != null && itemstack == null)
                            return false;

                        if (itemstack.getItem() != itemstack1.getItem())
                            return false;

                        if (itemstack.getItemDamage() != itemstack1.getItemDamage())
                            return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public ItemStack getCraftingResult(G55CraftingRecipe recipe) {
        return this.getRecipeOutput().copy();
    }

    @Override
    public int getRecipeSize() {
        return this.recipeWidth * this.recipeHeight * this.recipeDepth;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return this.recipeOutput;
    }
}
