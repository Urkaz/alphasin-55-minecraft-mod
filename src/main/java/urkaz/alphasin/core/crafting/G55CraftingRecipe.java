package urkaz.alphasin.core.crafting;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class G55CraftingRecipe {

    private ItemStack[] stackList = new ItemStack[8];

    public G55CraftingRecipe() {
    }

    public G55CraftingRecipe(ItemStack[] allSlots) {
        stackList = allSlots;
    }

    public static G55CraftingRecipe loadSynthesizerCraftingFromNBT(NBTTagCompound nbtTag) {
        G55CraftingRecipe sc = new G55CraftingRecipe();

        for (int i = 0; i < sc.getAllSlots().length; i++)
            sc.setItemInSlot(i, ItemStack.loadItemStackFromNBT(nbtTag.getCompoundTag(Integer.toString(i))));

        return sc;
    }

    public void setItemInSlot(int slot, ItemStack itemStack) {
        stackList[slot] = itemStack;
    }

    public void clearSlots() {
        for (int i = 0; i < stackList.length; i++)
            stackList[i] = null;
    }

    public ItemStack getItemInSlot(int slot) {
        return stackList[slot];
    }

    public ItemStack getItemInSlot(int slot, int rotation) {
        switch (rotation) {
            default:
            case 0:
                return stackList[slot];
            case 90:
                return getRotated90DegreeItems()[slot];
            case 180:
                return getRotated180DegreeItems()[slot];
            case 270:
                return getRotated270DegreeItems()[slot];
        }
    }

    public ItemStack[] getAllSlots() {
        return stackList;
    }

    public NBTTagCompound writeToNBT(NBTTagCompound nbtTag) {
        for (int i = 0; i < stackList.length; i++)
            if (stackList[i] != null)
                nbtTag.setTag(Integer.toString(i), stackList[i].writeToNBT(new NBTTagCompound()));

        return nbtTag;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof G55CraftingRecipe) {
            for (int i = 0; i < 8; i++) {
                if (this.getItemInSlot(i) != null && ((G55CraftingRecipe) obj).getItemInSlot(i) == null ||
                        this.getItemInSlot(i) == null && ((G55CraftingRecipe) obj).getItemInSlot(i) != null)
                    return false;
                if (this.getItemInSlot(i) != null && ((G55CraftingRecipe) obj).getItemInSlot(i) != null)
                    if (!this.getItemInSlot(i).isItemEqual(((G55CraftingRecipe) obj).getItemInSlot(i)))
                        return false;
            }
            return true;
        } else
            return false;
    }

    public ItemStack[] getRotated90DegreeItems() {
        return new ItemStack[]{
                stackList[2], stackList[0],
                stackList[3], stackList[1],
                stackList[6], stackList[4],
                stackList[7], stackList[5]};
    }

    public ItemStack[] getRotated180DegreeItems() {
        return new ItemStack[]{
                stackList[3], stackList[2],
                stackList[1], stackList[0],
                stackList[7], stackList[6],
                stackList[5], stackList[4]};
    }

    public ItemStack[] getRotated270DegreeItems() {
        return new ItemStack[]{
                stackList[1], stackList[3],
                stackList[0], stackList[2],
                stackList[5], stackList[7],
                stackList[4], stackList[6]};
    }
}
