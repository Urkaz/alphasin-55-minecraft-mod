package urkaz.alphasin.core.crafting;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import urkaz.alphasin.core.blocks.G55Blocks;
import urkaz.alphasin.core.item.G55Items;

import java.util.*;

public class G55CraftingManager {

    private static G55CraftingManager instance;
    private List<IG55Recipe> recipes = new ArrayList<IG55Recipe>();

    private ItemStack aggregate0 = new ItemStack(G55Blocks.aggregateBlock, 1, 0);

    public static G55CraftingManager getInstance() {
        return init();
    }

    public static G55CraftingManager init() {
        if (instance == null) {
            instance = new G55CraftingManager();
            instance.registerRecipes();
        }
        return instance;
    }

    private void registerRecipes() {
        /*
        INVALID
        this.addRecipe(new ItemStack(G55Blocks.aggregateBlock, 1, 0), new Object[]{"D D", "D D", 'D', new ItemStack(G55Blocks.dirtBlock, 1, 0)});
         */
        this.addRecipe(new ItemStack(G55Blocks.aggregateBlock, 1, 1), new Object[]{"S", "A", 'A', aggregate0, 'S', new ItemStack(G55Blocks.stainBlock, 1, 1)});
        this.addRecipe(new ItemStack(G55Blocks.aggregateBlock, 1, 2), new Object[]{"S", "A", 'A', aggregate0, 'S', new ItemStack(G55Blocks.stainBlock, 1, 2)});
        this.addRecipe(new ItemStack(G55Blocks.aggregateBlock, 1, 3), new Object[]{"S", "A", 'A', aggregate0, 'S', new ItemStack(G55Blocks.stainBlock, 1, 3)});
        this.addRecipe(new ItemStack(G55Blocks.aggregateBlock, 1, 4), new Object[]{"S", "A", 'A', aggregate0, 'S', new ItemStack(G55Blocks.stainBlock, 1, 4)});
        this.addRecipe(new ItemStack(G55Blocks.aggregateBlock, 1, 5), new Object[]{"S", "A", 'A', aggregate0, 'S', new ItemStack(G55Blocks.stainBlock, 1, 5)});

        this.addRecipe(aggregate0, new Object[]{"DD", "DD", 'D', new ItemStack(G55Blocks.dirtBlock, 1, 0)});

        this.addRecipe(aggregate0, new Object[]{"DDDD", "DDDD", 'D', new ItemStack(G55Blocks.dirtBlock, 1, 0)});
        this.addRecipe(aggregate0, new Object[]{"DDDD", "DDDD", 'D', new ItemStack(G55Blocks.dirtBlock, 1, 1)});
        this.addRecipe(aggregate0, new Object[]{"DDDD", "DDDD", 'D', new ItemStack(G55Blocks.dirtBlock, 1, 2)});
        this.addRecipe(aggregate0, new Object[]{"DDDD", "DDDD", 'D', new ItemStack(G55Blocks.dirtBlock, 1, 3)});

        this.addShapelessRecipe(new ItemStack(G55Items.itemCharge, 5, 0), new Object[]{new ItemStack(G55Blocks.citronoxBlock, 1, 0)});
        this.addShapelessRecipe(new ItemStack(G55Blocks.minIngotBlock, 1, 1), new Object[]{G55Blocks.mineralIron});
        this.addShapelessRecipe(new ItemStack(G55Blocks.minIngotBlock, 1, 0), new Object[]{G55Blocks.mineralAluminium});
        this.addShapelessRecipe(new ItemStack(G55Blocks.blindage, 1, 0), new Object[]{new ItemStack(G55Blocks.minIngotBlock, 1, 0)});

        this.addShapelessRecipe(new ItemStack(G55Blocks.woodPlank, 2, 0), new Object[]{new ItemStack(G55Blocks.woodBlock, 1, 0)});
        this.addShapelessRecipe(new ItemStack(G55Blocks.woodPlank, 2, 0), new Object[]{new ItemStack(G55Blocks.woodBlock, 1, 1)});
        this.addShapelessRecipe(new ItemStack(G55Blocks.woodPlank, 2, 0), new Object[]{new ItemStack(G55Blocks.woodBlock, 1, 2)});
        this.addShapelessRecipe(new ItemStack(G55Blocks.woodPlank, 2, 0), new Object[]{new ItemStack(G55Blocks.woodBlock, 1, 3)});

        //this.addRecipe(new ItemStack(G55Blocks.blindage, 1, 0), new Object[]{"AB", "AB", 'A', new ItemStack(G55Blocks.minIngotBlock, 1, 0), 'B', new ItemStack(G55Blocks.minIngotBlock, 1, 1)});
        //this.addRecipe(new ItemStack(G55Blocks.aggregateBlock, 1, 3), new Object[]{"SS", "AA", 'A', new ItemStack(G55Blocks.aggregateBlock, 1, 0), 'S', new ItemStack(G55Blocks.stainBlock, 1, 1)});
        //this.addRecipe(new ItemStack(G55Blocks.grassBlock, 1, 1), new Object[]{"DADA", 'D', new ItemStack(G55Blocks.dirtBlock, 1, 0), 'A', new ItemStack(G55Blocks.blindage, 1, 0)});
        //this.addRecipe(new ItemStack(G55Blocks.rockBlock, 1, 0), new Object[]{"D   ", "ABC ", 'D', G55Blocks.blindage, 'B', G55Blocks.mineralAluminium, 'C', G55Blocks.mineralIron, 'A', Blocks.redstone_block});

        Collections.sort(this.recipes, new Comparator<IG55Recipe>() {
            public int compare(IG55Recipe recipe1, IG55Recipe recipe2) {
                return recipe1 instanceof ShapelessRecipe && recipe2 instanceof ShapedRecipe ? 1 :
                        (recipe2 instanceof ShapelessRecipe && recipe1 instanceof ShapedRecipe ? -1 :
                                (recipe2.getRecipeSize() < recipe1.getRecipeSize() ? -1 :
                                        recipe2.getRecipeSize() > recipe1.getRecipeSize() ? 1 : 0));
            }
        });
    }

    public ShapedRecipe addRecipe(ItemStack output, Object[] input) {
        String recipeStr = "";
        int i = 0;
        int width = 0;
        int height = 0;
        int depth = 1;

        if (input[i] instanceof String[]) {
            String[] recipeStrArray = (String[]) input[i++];

            for (String partialRecipeStr : recipeStrArray) {
                ++height;
                width = partialRecipeStr.length();
                if (width > 2)
                    depth = 2;
                recipeStr = recipeStr + partialRecipeStr;
            }
        } else {
            while (input[i] instanceof String) {
                String partialRecipeStr = (String) input[i++];
                ++height;
                width = partialRecipeStr.length();
                if (width > 2)
                    depth = 2;
                recipeStr = recipeStr + partialRecipeStr;
            }
        }

        HashMap<Character, ItemStack> ingredientDictionary;

        for (ingredientDictionary = new HashMap<Character, ItemStack>(); i < input.length; i += 2) {
            Character charKey = (Character) input[i];
            ItemStack itemStack = null;

            if (input[i + 1] instanceof Block)
                itemStack = new ItemStack((Block) input[i + 1], 1, 0);
            else if (input[i + 1] instanceof ItemStack)
                itemStack = (ItemStack) input[i + 1];

            ingredientDictionary.put(charKey, itemStack);
        }

        ItemStack[] itemStackInput = new ItemStack[width * height];

        for (int index = 0; index < width * height; ++index) {
            char charKey = recipeStr.charAt(index);

            if (ingredientDictionary.containsKey(Character.valueOf(charKey)))
                itemStackInput[index] = ingredientDictionary.get(Character.valueOf(charKey)).copy();
            else
                itemStackInput[index] = null;
        }

        ShapedRecipe shapedRecipe = new ShapedRecipe((width + depth) % 2 + depth, height, depth, itemStackInput, output);
        this.recipes.add(shapedRecipe);
        return shapedRecipe;
    }

    private void addShapelessRecipe(ItemStack output, Object[] input) {
        ArrayList<ItemStack> inputList = new ArrayList<ItemStack>();
        int i = input.length;

        for (Object object1 : input) {
            if (object1 instanceof ItemStack)
                inputList.add(((ItemStack) object1).copy());
            else {
                if (!(object1 instanceof Block))
                    throw new RuntimeException("Invalid shapeless recipe.");

                inputList.add(new ItemStack((Block) object1));
            }
        }

        this.recipes.add(new ShapelessRecipe(output, inputList));
    }

    public ItemStack findMatchingRecipe(G55CraftingRecipe craftingRecipe) {
        for (IG55Recipe ig55Recipe : this.recipes) {
            if (ig55Recipe.matches(craftingRecipe))
                return ig55Recipe.getCraftingResult(craftingRecipe);
        }
        return null;
    }
}
