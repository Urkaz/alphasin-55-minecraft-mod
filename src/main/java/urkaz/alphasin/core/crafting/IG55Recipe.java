package urkaz.alphasin.core.crafting;

import net.minecraft.item.ItemStack;

public interface IG55Recipe {
    /**
     * Used to check if a recipe matches current crafting inventory
     */
    boolean matches(G55CraftingRecipe recipe);

    /**
     * Returns an Item that is the result of this recipe
     */
    ItemStack getCraftingResult(G55CraftingRecipe recipe);

    /**
     * Returns the size of the recipe area
     */
    int getRecipeSize();

    ItemStack getRecipeOutput();
}
