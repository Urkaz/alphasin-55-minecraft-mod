package urkaz.alphasin.core;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import micdoodle8.mods.galacticraft.api.galaxies.*;
import micdoodle8.mods.galacticraft.api.vector.Vector3;
import micdoodle8.mods.galacticraft.api.world.IAtmosphericGas;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import urkaz.alphasin.core.blocks.G55Blocks;
import urkaz.alphasin.core.crafting.G55CraftingManager;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.entity.G55Entities;
import urkaz.alphasin.core.event.EventHandlerG55;
import urkaz.alphasin.core.item.G55Items;
import urkaz.alphasin.core.network.ChargesPacket;
import urkaz.alphasin.core.network.PacketHandler;
import urkaz.alphasin.core.network.TileEntityMultiPacket;
import urkaz.alphasin.core.proxy.CommonProxy;
import urkaz.alphasin.core.tile.G55TileEntities;
import urkaz.alphasin.core.util.References;
import urkaz.alphasin.core.world.gen.G55OreGenerator;

@Mod(modid = References.MODID, name = References.MODNAME, version = References.MODVER, dependencies = "required-after:GalacticraftCore")
public class Galaxy55Core {

    @Instance(value = References.MODID)
    public static Galaxy55Core instance;

    @SidedProxy(clientSide = References.ClientProxy_Location, serverSide = References.CommonProxy_Location)
    public static CommonProxy proxy;

    public static SimpleNetworkWrapper network;

    public static SolarSystem solarSystemGalaxy55;
    public static Planet redEarth;
    public static Planet barrenSands;
    public static Planet acidForest;
    public static Planet winterPeaks;
    public static Moon darkRocksMoon;
    public static Satellite spaceShipStation;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        G55CreativeTab.init();
        G55Blocks.registerBlocks();
        G55TileEntities.registerTileEntities();
        G55Items.registerItems();
        G55Entities.registerEntities();

        G55OreGenerator ore_gen = new G55OreGenerator();
        GameRegistry.registerWorldGenerator(ore_gen, 0);

        proxy.registerRenderers();

        network = NetworkRegistry.INSTANCE.newSimpleChannel("G55Channel");
        network.registerMessage(PacketHandler.ChargesHandler.class, ChargesPacket.class, 0, Side.CLIENT);
        network.registerMessage(PacketHandler.TileEntityMultiHandler.class, TileEntityMultiPacket.class, 1, Side.SERVER);
    }

    @EventHandler
    public void load(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(new EventHandlerG55());
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        Galaxy55Core.proxy.registerRenderers();

        Galaxy55Core.solarSystemGalaxy55 = new SolarSystem("g55system", "galaxy55").setMapPosition(new Vector3(1.0F, 1.0F, 0.0F));
        Star starG55 = (Star) new Star("g55star").setParentSolarSystem(Galaxy55Core.solarSystemGalaxy55).setTierRequired(-1);
        starG55.setBodyIcon(new ResourceLocation(References.Assets_Prefix, "textures/gui/celestialbodies/sun.png"));
        Galaxy55Core.solarSystemGalaxy55.setMainStar(starG55);

        redEarth = (Planet) new Planet("redEarth").setParentSolarSystem(Galaxy55Core.solarSystemGalaxy55).setRingColorRGB(0.1F, 0.9F, 0.6F).setPhaseShift(0.0F);
        redEarth.setBodyIcon(new ResourceLocation(References.Assets_Prefix, "textures/gui/celestialbodies/planet_red.png"));
        redEarth.setTierRequired(1);
        redEarth.atmosphereComponent(IAtmosphericGas.NITROGEN).atmosphereComponent(IAtmosphericGas.OXYGEN).atmosphereComponent(IAtmosphericGas.ARGON).atmosphereComponent(IAtmosphericGas.WATER);

        barrenSands = (Planet) new Planet("barrenSands").setParentSolarSystem(Galaxy55Core.solarSystemGalaxy55).setRingColorRGB(0.1F, 0.9F, 0.6F).setPhaseShift(0.0F);
        barrenSands.setRelativeDistanceFromCenter(new CelestialBody.ScalableDistance(0.4F, 0.4F)).setRelativeOrbitTime(0.3F);
        barrenSands.setBodyIcon(new ResourceLocation(References.Assets_Prefix, "textures/gui/celestialbodies/planet_sands.png"));
        //barrenSands.setTierRequired(2);
        //barrenSands.atmosphereComponent(IAtmosphericGas.NITROGEN).atmosphereComponent(IAtmosphericGas.OXYGEN).atmosphereComponent(IAtmosphericGas.ARGON).atmosphereComponent(IAtmosphericGas.WATER);

        acidForest = (Planet) new Planet("acidForest").setParentSolarSystem(Galaxy55Core.solarSystemGalaxy55).setRingColorRGB(0.1F, 0.9F, 0.6F).setPhaseShift(0.0F);
        acidForest.setRelativeDistanceFromCenter(new CelestialBody.ScalableDistance(1.2F, 1.2F)).setRelativeOrbitTime(1.2F);
        acidForest.setBodyIcon(new ResourceLocation(References.Assets_Prefix, "textures/gui/celestialbodies/planet_forest.png"));
        //acidForest.setTierRequired(2);
        //acidForest.atmosphereComponent(IAtmosphericGas.NITROGEN).atmosphereComponent(IAtmosphericGas.OXYGEN).atmosphereComponent(IAtmosphericGas.ARGON).atmosphereComponent(IAtmosphericGas.WATER);

        winterPeaks = (Planet) new Planet("winterPeaks").setParentSolarSystem(Galaxy55Core.solarSystemGalaxy55).setRingColorRGB(0.1F, 0.9F, 0.6F).setPhaseShift(0.0F);
        winterPeaks.setRelativeDistanceFromCenter(new CelestialBody.ScalableDistance(2.0F, 2.0F)).setRelativeOrbitTime(4F);
        winterPeaks.setBodyIcon(new ResourceLocation(References.Assets_Prefix, "textures/gui/celestialbodies/planet_winter.png"));
        //winterPeaks.setTierRequired(2);
        //winterPeaks.atmosphereComponent(IAtmosphericGas.NITROGEN).atmosphereComponent(IAtmosphericGas.OXYGEN).atmosphereComponent(IAtmosphericGas.ARGON).atmosphereComponent(IAtmosphericGas.WATER);

        darkRocksMoon = (Moon) (new Moon("darkRocksMoon")).setParentPlanet(redEarth).setRelativeSize(0.2667F).setRelativeDistanceFromCenter(new CelestialBody.ScalableDistance(13.0F, 13.0F)).setRelativeOrbitTime(100.0F);
        //darkRocksMoon.setDimensionInfo(ConfigManagerCore.idDimensionMoon, WorldProviderMoon.class).setTierRequired(1);
        darkRocksMoon.setBodyIcon(new ResourceLocation(References.Assets_Prefix, "textures/gui/celestialbodies/planet_dark.png"));

        spaceShipStation = (Satellite) (new Satellite("spaceStation.ship")).setParentBody(redEarth).setRelativeSize(0.2667F).setRelativeDistanceFromCenter(new CelestialBody.ScalableDistance(9.0F, 9.0F)).setRelativeOrbitTime(20.0F);
        //spaceShipStation.setDimensionInfo(ConfigManagerCore.idDimensionOverworldOrbit, WorldProviderOrbit.class, false).setTierRequired(1);
        spaceShipStation.setBodyIcon(new ResourceLocation(References.Assets_Prefix, "textures/gui/celestialbodies/spaceShip.png"));

        GalaxyRegistry.registerSolarSystem(Galaxy55Core.solarSystemGalaxy55);
        GalaxyRegistry.registerPlanet(redEarth);
        GalaxyRegistry.registerPlanet(barrenSands);
        GalaxyRegistry.registerPlanet(acidForest);
        GalaxyRegistry.registerPlanet(winterPeaks);
        GalaxyRegistry.registerMoon(darkRocksMoon);
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        G55CraftingManager.init();
        Galaxy55Core.proxy.registerGUI();
    }
}
