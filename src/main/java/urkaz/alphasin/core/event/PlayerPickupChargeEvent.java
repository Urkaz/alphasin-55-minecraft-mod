package urkaz.alphasin.core.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.PlayerEvent;
import urkaz.alphasin.core.util.ChargeType;

public class PlayerPickupChargeEvent extends PlayerEvent {

    public final ChargeType charge;

    public PlayerPickupChargeEvent(EntityPlayer player, ChargeType charge) {
        super(player);
        this.charge = charge;
    }
}
