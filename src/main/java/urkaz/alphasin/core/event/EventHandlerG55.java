package urkaz.alphasin.core.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import urkaz.alphasin.core.blocks.G55Blocks;
import urkaz.alphasin.core.entity.charges.EntityCharge;
import urkaz.alphasin.core.entity.player.ChargesProperties;
import urkaz.alphasin.core.util.ChargeType;

public class EventHandlerG55 {

    @SubscribeEvent
    public void onEntityConstructing(EntityEvent.EntityConstructing event) {
        if (event.entity instanceof EntityPlayer)
            ChargesProperties.register((EntityPlayer) event.entity);
    }

    @SubscribeEvent
    public void onEntityJoinWorld(EntityJoinWorldEvent event) {
        if (!event.entity.worldObj.isRemote && event.entity instanceof EntityPlayer)
            ChargesProperties.get((EntityPlayer) event.entity).sync();
    }

    @SubscribeEvent
    public void onPlayerDeath(PlayerDropsEvent event) {
        if (event.entityPlayer instanceof EntityPlayerMP) {
            ChargesProperties prop = ChargesProperties.get(event.entityPlayer);
            for (int i = 0; i < prop.getAllCharges().length; i++) {
                if (prop.getAllCharges()[i] > 0) {
                    EntityCharge.spawnChargesInWorld(
                            ((EntityPlayerMP) event.entityPlayer).worldObj,
                            ((EntityPlayerMP) event.entityPlayer).posX,
                            ((EntityPlayerMP) event.entityPlayer).posY,
                            ((EntityPlayerMP) event.entityPlayer).posZ,
                            ChargeType.values()[i],
                            prop.getAllCharges()[i]);
                }
            }
        }
    }

    @SubscribeEvent
    public void onPlayerPickupCharge(PlayerPickupChargeEvent event) {
        if (event.entityPlayer instanceof EntityPlayerMP) {
            ChargesProperties prop = ChargesProperties.get(event.entityPlayer);
            prop.addCharge(event.charge, 1);
        }
    }

    @SubscribeEvent
    public void onBlockBreakOnCraftingPanel(PlayerInteractEvent event) {
        if (event.action == PlayerInteractEvent.Action.LEFT_CLICK_BLOCK) {
            final Block cp = G55Blocks.craftingPanel;
            final Block cpf = G55Blocks.craftingPanelFull;
            final Block fb = G55Blocks.fakeBlock;

            for (int i = 1; i <= 2; i++) {
                if (event.entityPlayer.worldObj.getBlock(event.x, event.y - i, event.z) == fb &&
                        event.entityPlayer.worldObj.getBlockMetadata(event.x, event.y - i, event.z) == 0 ||
                        event.entityPlayer.worldObj.getBlock(event.x, event.y - i, event.z) == cp ||
                        event.entityPlayer.worldObj.getBlock(event.x, event.y - i, event.z) == cpf)
                    event.entityPlayer.worldObj.func_147480_a(event.x, event.y, event.z, true);
            }
        }
    }
}
