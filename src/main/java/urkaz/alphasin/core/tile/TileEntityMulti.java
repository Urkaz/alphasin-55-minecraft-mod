package urkaz.alphasin.core.tile;

import cpw.mods.fml.relauncher.Side;
import micdoodle8.mods.galacticraft.api.vector.BlockVec3;
import micdoodle8.mods.miccore.Annotations;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.network.TileEntityMultiPacket;

public class TileEntityMulti extends TileEntity {

    public BlockVec3 mainBlockPosition;

    public TileEntityMulti() {}

    public void setMainBlock(BlockVec3 mainBlock) {
        this.mainBlockPosition = mainBlock;

        //if(!this.worldObj.isRemote) {
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
            sync();
        //}
    }

    public void onBlockRemoval() {
        if (this.mainBlockPosition != null) {
            TileEntity tile = this.worldObj.getTileEntity(this.mainBlockPosition.x, this.mainBlockPosition.y, this.mainBlockPosition.z);

            if (tile instanceof IMultiBlock) {
                IMultiBlock mainBlock = (IMultiBlock) tile;
                mainBlock.onDestroy(this);
            }
        }
    }

    /*public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer entityPlayer, int side) {
        if (this.mainBlockPosition != null) {
            TileEntity tileEntity = this.worldObj.getTileEntity(this.mainBlockPosition.x, this.mainBlockPosition.y, this.mainBlockPosition.z);

            if (tileEntity instanceof IMultiBlock)
                return ((IMultiBlock) tileEntity).onActivated(entityPlayer, side);
        }
        return false;
    }*/

    /*public TileEntity getMainBlockTile() {
        if (this.mainBlockPosition != null)
            return this.worldObj.getTileEntity(this.mainBlockPosition.x, this.mainBlockPosition.y, this.mainBlockPosition.z);
        return null;
    }*/

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.mainBlockPosition = new BlockVec3(nbt.getCompoundTag("mainBlockPosition"));
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.mainBlockPosition != null)
            nbt.setTag("mainBlockPosition", this.mainBlockPosition.writeToNBT(new NBTTagCompound()));
    }

    public final void sync() {
        if (!this.worldObj.isRemote) {
            Galaxy55Core.network.sendToAll(new TileEntityMultiPacket(new BlockVec3(xCoord,yCoord,zCoord), mainBlockPosition));
        }
    }
}