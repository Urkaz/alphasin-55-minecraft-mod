package urkaz.alphasin.core.tile;

import micdoodle8.mods.galacticraft.api.vector.BlockVec3;
import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.entity.charges.EntityCharge;
import urkaz.alphasin.core.item.ItemCharge;
import urkaz.alphasin.core.util.BlockFacing;

public class TileEntitySynthesizer extends TileEntity implements IMultiBlock {

    private BlockVec3 linkedPanel;
    private ItemStack output;

    @Override
    public boolean onActivated(EntityPlayer entityPlayer, int side) {
        if (linkedPanel != null) {
            if (output != null) {
                TileEntityCraftingPanel tile = (TileEntityCraftingPanel) this.worldObj.getTileEntity(linkedPanel.x, linkedPanel.y, linkedPanel.z);

                if (!this.worldObj.isRemote) {

                    BlockFacing so = BlockFacing.getFromSide(side);

                    float lx, lz;
                    float ly = 0.5F;
                    float mx = 0;
                    float my = 0;
                    float mz = 0;

                    if (so.facingY == 0) {
                        lx = 0.5F + so.facingX * 1F;
                        lz = 0.5F + so.facingZ * 1F;

                        mx = so.facingX == 0 ? (this.worldObj.rand.nextFloat() - this.worldObj.rand.nextFloat()) * 0.03F : so.facingX * 0.13F;
                        mz = so.facingZ == 0 ? (this.worldObj.rand.nextFloat() - this.worldObj.rand.nextFloat()) * 0.03F : so.facingZ * 0.13F;
                    } else {
                        lx = 0.5F;
                        ly = so.facingY == 1 ? 1.5F : -0.5F;
                        lz = 0.5F;

                        my = so.facingY == 1 ? 0.3F : 0;
                    }

                    if (!ItemCharge.isCharge(output.getItem())) {
                        EntityItem itemEntity = new EntityItem(this.getWorldObj(), this.xCoord + lx, this.yCoord + ly, this.zCoord + lz, output.copy());

                        itemEntity.motionX = mx;
                        itemEntity.motionY = my;
                        itemEntity.motionZ = mz;

                        this.getWorldObj().spawnEntityInWorld(itemEntity);
                    } else {
                        EntityCharge.spawnChargesInWorldWithMotion(this.getWorldObj(), this.xCoord + lx, this.yCoord + ly, this.zCoord + lz, ItemCharge.getChargeType(output.getItemDamage()), output.stackSize, mx, my, mz);
                    }
                }

                if (!entityPlayer.capabilities.isCreativeMode) {
                    for (int i = 0; i < tile.getCraftingRecipe().getAllSlots().length; i++) {
                        if (tile.getCraftingRecipe().getItemInSlot(i) != null) {
                            if (!deleteItemStackFromInventory(entityPlayer, tile.getCraftingRecipe().getItemInSlot(i))) {
                                int x = this.linkedPanel.x + i % 2;
                                int y = this.linkedPanel.y + 1 + i / 4;
                                int z = this.linkedPanel.z + (i / 2) % 2;

                                Block block = this.worldObj.getBlock(x, y, z);
                                Galaxy55Core.proxy.renderDestroyEffects(x, y, z,
                                        block, Block.getIdFromBlock(block) >> 12 & 255);
                                this.worldObj.setBlockToAir(x, y, z);
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public void linkCraftingPanel(BlockVec3 craftingPanelPosition) {
        linkedPanel = craftingPanelPosition;
        System.out.println("linked with: " + linkedPanel);
    }

    public void unlinkCraftingPanel() {
        linkedPanel = null;
        output = null;
        System.out.println("unlinked");
        this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
    }

    public boolean isLinked() {
        return linkedPanel != null;
    }

    @Override
    public void onCreate(BlockVec3 craftingPanelPosition) {
    }

    @Override
    public void onDestroy(TileEntity callingBlock) {
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.linkedPanel = new BlockVec3(nbt.getCompoundTag("linkedPanelPosition"));
        this.output = ItemStack.loadItemStackFromNBT(nbt.getCompoundTag("output"));
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.linkedPanel != null)
            nbt.setTag("linkedPanelPosition", this.linkedPanel.writeToNBT(new NBTTagCompound()));
        if (this.output != null)
            nbt.setTag("output", output.writeToNBT(new NBTTagCompound()));
    }

    private boolean deleteItemStackFromInventory(EntityPlayer entityPlayer, ItemStack itemToDelete) {
        if (!entityPlayer.worldObj.isRemote) {
            if (itemToDelete != null) {
                int metaDel = itemToDelete.getItemDamage();
                Item itemDel = itemToDelete.getItem();

                for (int i = 0; i < entityPlayer.inventory.mainInventory.length; i++) {
                    if (entityPlayer.inventory.mainInventory[i] != null) {
                        int meta = entityPlayer.inventory.mainInventory[i].getItemDamage();
                        Item item = entityPlayer.inventory.mainInventory[i].getItem();

                        if (item == itemDel && meta == metaDel) {
                            if (--entityPlayer.inventory.mainInventory[i].stackSize <= 0)
                                entityPlayer.inventory.mainInventory[i] = null;

                            entityPlayer.inventoryContainer.detectAndSendChanges();
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public ItemStack getOutput() {
        return output;
    }

    public void setOutput(ItemStack outputStack) {
        if (!ItemStack.areItemStacksEqual(output, outputStack)) {
            output = outputStack;
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
        }
    }

    @Override
    public Packet getDescriptionPacket() {
        NBTTagCompound nbtTag = new NBTTagCompound();
        writeToNBT(nbtTag);
        return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
        readFromNBT(packet.func_148857_g());
    }
}
