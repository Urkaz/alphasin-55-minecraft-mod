package urkaz.alphasin.core.tile;

import micdoodle8.mods.galacticraft.api.vector.BlockVec3;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import urkaz.alphasin.core.blocks.G55Blocks;

import java.util.ArrayList;

public class TileEntityCraftingPanelSingle extends TileEntity {

    @Override
    public void updateEntity() {
        if (!this.worldObj.isRemote) {

            final ArrayList<TileEntity> attachedCraftingPanels = new ArrayList<TileEntity>();

            for (int x = this.xCoord; x < this.xCoord + 2; x++) {
                for (int z = this.zCoord; z < this.zCoord + 2; z++) {
                    final TileEntity tile = this.worldObj.getTileEntity(x, this.yCoord, z);

                    if (tile instanceof TileEntityCraftingPanelSingle) {
                        attachedCraftingPanels.add(tile);
                    }
                }
            }

            if (attachedCraftingPanels.size() == 4) {
                for (final TileEntity tile : attachedCraftingPanels) {
                    tile.invalidate();
                    tile.getWorldObj().setBlock(tile.xCoord, tile.yCoord, tile.zCoord, Blocks.air);
                }

                this.worldObj.setBlock(this.xCoord, this.yCoord, this.zCoord, G55Blocks.craftingPanelFull);
                final TileEntityCraftingPanel tile = (TileEntityCraftingPanel) this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord);

                if (tile != null) {
                    tile.onCreate(new BlockVec3(this.xCoord, this.yCoord, this.zCoord));
                }
            }
        }
    }
}
