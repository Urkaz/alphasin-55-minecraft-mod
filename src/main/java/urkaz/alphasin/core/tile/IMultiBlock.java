package urkaz.alphasin.core.tile;

import micdoodle8.mods.galacticraft.api.vector.BlockVec3;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;

public interface IMultiBlock {

    public boolean onActivated(EntityPlayer entityPlayer, int side);

    public void onCreate(BlockVec3 placedPosition);

    public void onDestroy(TileEntity callingBlock);
}
