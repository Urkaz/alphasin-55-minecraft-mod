package urkaz.alphasin.core.tile;

import cpw.mods.fml.common.registry.GameRegistry;


public class G55TileEntities {

    public static void registerTileEntities() {
        GameRegistry.registerTileEntity(TileEntityCraftingPanelSingle.class, "Crafting Panel Single");
        GameRegistry.registerTileEntity(TileEntityCraftingPanel.class, "Crafting Panel Full");
        GameRegistry.registerTileEntity(TileEntitySynthesizer.class, "Synthesizer");
        GameRegistry.registerTileEntity(TileEntityMulti.class, "MultiTileEntity");
    }
}
