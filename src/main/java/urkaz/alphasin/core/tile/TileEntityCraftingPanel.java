package urkaz.alphasin.core.tile;

import micdoodle8.mods.galacticraft.api.vector.BlockVec3;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.blocks.BlockMulti;
import urkaz.alphasin.core.blocks.G55Blocks;
import urkaz.alphasin.core.crafting.G55CraftingManager;
import urkaz.alphasin.core.crafting.G55CraftingRecipe;

public class TileEntityCraftingPanel extends TileEntityMulti implements IMultiBlock {

    G55CraftingRecipe lastRecipe;
    private BlockVec3 linkedSynthesizer = null;
    private G55CraftingRecipe craftingRecipe = new G55CraftingRecipe();

    @Override
    public void updateEntity() {
        super.updateEntity();

        if (!this.worldObj.isRemote) {
            if (linkedSynthesizer == null) {

                panelsLoop:
                for (int y = 1; y <= 2; y++) { //Height

                    for (int i = -1; i <= 2; i += 3) { //Wide/Long
                        for (int j = 0; j <= 1; j++) { //Long/Wide

                            //Wide
                            TileEntity tile = this.worldObj.getTileEntity(this.xCoord + i, this.yCoord + y, this.zCoord + j);
                            if (tile instanceof TileEntitySynthesizer) {
                                if (!((TileEntitySynthesizer) tile).isLinked()) {
                                    ((TileEntitySynthesizer) tile).linkCraftingPanel(new BlockVec3(this));
                                    this.linkedSynthesizer = new BlockVec3(tile);
                                    this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
                                    break panelsLoop;
                                }
                            }

                            //Long
                            tile = this.worldObj.getTileEntity(this.xCoord + j, this.yCoord + y, this.zCoord + i);
                            if (tile instanceof TileEntitySynthesizer) {
                                if (!((TileEntitySynthesizer) tile).isLinked()) {
                                    ((TileEntitySynthesizer) tile).linkCraftingPanel(new BlockVec3(this));
                                    this.linkedSynthesizer = new BlockVec3(tile);
                                    this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
                                    break panelsLoop;
                                }
                            }
                        }
                    }
                }
            } else {
                //Check synthesizer
                TileEntity tile = this.worldObj.getTileEntity(this.linkedSynthesizer.x, this.linkedSynthesizer.y, this.linkedSynthesizer.z);
                if (tile == null || !(tile instanceof TileEntitySynthesizer)) {
                    this.linkedSynthesizer = null;

                    this.craftingRecipe.clearSlots();
                    this.lastRecipe = null;

                    this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
                    return;
                }

                //Old recipe and new one
                lastRecipe = new G55CraftingRecipe(craftingRecipe.getAllSlots().clone());
                craftingRecipe.clearSlots();

                //Get crafting items
                int index = 7;
                for (int y = 1; y < 3; y++) {
                    for (int z = 0; z < 2; z++) {
                        for (int x = 0; x < 2; x++) {
                            Block b = this.getWorldObj().getBlock(this.xCoord + x, this.yCoord + y, this.zCoord + z);
                            int meta = this.getWorldObj().getBlockMetadata(this.xCoord + x, this.yCoord + y, this.zCoord + z);
                            if (!(b instanceof BlockAir)) {
                                ItemStack is = new ItemStack(b, 1, meta);
                                craftingRecipe.setItemInSlot(index, is);
                            }
                            index--;
                        }
                    }
                }

                //Compare old recipe with the current. If they aren't the same, set the output on the linked Synthesizer.
                if (!craftingRecipe.equals(lastRecipe))
                    ((TileEntitySynthesizer) tile).setOutput(G55CraftingManager.getInstance().findMatchingRecipe(craftingRecipe));
            }
        }
    }

    public G55CraftingRecipe getCraftingRecipe() {
        return craftingRecipe;
    }

    @Override
    public boolean onActivated(EntityPlayer entityPlayer, int side) {
        return false;
    }

    @Override
    public void onCreate(BlockVec3 placedPosition) {
        this.mainBlockPosition = placedPosition;
        this.markDirty();

        for (int x = 0; x < 2; x++) {
            for (int z = 0; z < 2; z++) {
                final BlockVec3 vecToAdd = new BlockVec3(placedPosition.x + x, placedPosition.y, placedPosition.z + z);

                if (!vecToAdd.equals(placedPosition)) {
                    this.worldObj.setBlock(vecToAdd.x, vecToAdd.y, vecToAdd.z, G55Blocks.fakeBlock, 0, 3);
                    TileEntityMulti tem = ((TileEntityMulti) this.worldObj.getTileEntity(vecToAdd.x, vecToAdd.y, vecToAdd.z));
                    tem.setMainBlock(placedPosition);
                }
                    //((BlockMulti) G55Blocks.fakeBlock).makeFakeBlock(this.worldObj, vecToAdd, placedPosition, 0);
            }
        }
    }

    @Override
    public void onDestroy(TileEntity callingBlock) {
        final BlockVec3 thisBlock = new BlockVec3(this);
        this.worldObj.func_147480_a(thisBlock.x, thisBlock.y, thisBlock.z, true);

        for (int x = 0; x < 2; x++) {
            for (int z = 0; z < 2; z++) {
                //TODO Fix destroy effect. When on server is not displaying, and locally shows the effect for metadata 0.
                Galaxy55Core.proxy.renderDestroyEffects(thisBlock.x + x, thisBlock.y, thisBlock.z + z,
                        G55Blocks.craftingPanel, Block.getIdFromBlock(G55Blocks.craftingPanel) >> 12 & 255);

                this.worldObj.setBlockToAir(thisBlock.x + x, thisBlock.y, thisBlock.z + z);
            }
        }

        for (int y = 1; y < 3; y++) {
            for (int z = 0; z < 2; z++) {
                for (int x = 0; x < 2; x++) {
                    this.worldObj.func_147480_a(thisBlock.x + x, thisBlock.y + y, thisBlock.z + z, true);
                }
            }
        }

        if (linkedSynthesizer != null)
            ((TileEntitySynthesizer) this.worldObj.getTileEntity(linkedSynthesizer.x, linkedSynthesizer.y, linkedSynthesizer.z)).unlinkCraftingPanel();
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.linkedSynthesizer = new BlockVec3(nbt.getCompoundTag("linkedSynthesizerPosition"));
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (this.linkedSynthesizer != null)
            nbt.setTag("linkedSynthesizerPosition", this.linkedSynthesizer.writeToNBT(new NBTTagCompound()));
    }

    @Override
    public Packet getDescriptionPacket() {
        NBTTagCompound nbtTag = new NBTTagCompound();
        writeToNBT(nbtTag);
        return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
        readFromNBT(packet.func_148857_g());
    }
}
