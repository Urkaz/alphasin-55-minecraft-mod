package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.blocks.items.ISubLocalization;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

import java.util.List;

public class MetaBlockIngot extends Block implements ISubLocalization {

    @SideOnly(Side.CLIENT)
    private IIcon[] textures;
    private String[] subNames;

    public MetaBlockIngot(String name, String[] subNames) {
        super(Material.iron);
        this.setHardness(2f);
        this.setResistance(30f);
        this.setStepSound(Block.soundTypeMetal);
        this.setBlockName(name);
        this.subNames = subNames;

        this.setBlockBounds(0.25f, 0f, 0.406f, 0.75f, 1f, 0.594f);
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public String getUnlocalizedName(String baseName, ItemStack itemStack) {
        int meta = itemStack.getItemDamage();

        return baseName + "." + subNames[meta];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < subNames.length; ++i) {
            list.add(new ItemStack(block, 1, i));
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        textures = new IIcon[subNames.length * 2];

        for (int i = 0; i < subNames.length; ++i) {
            textures[i * 2] = iconRegister.registerIcon(References.Texture_Prefix + subNames[i]);
            textures[i * 2 + 1] = iconRegister.registerIcon(References.Texture_Prefix + subNames[i] + "_side");
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if (side == 2 || side == 3)
            return textures[meta * 2];
        else
            return textures[meta * 2 + 1];
    }

    @Override
    public int getRenderType() {
        return Galaxy55Core.proxy.getBlockRender(this);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
