package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import urkaz.alphasin.core.blocks.items.ISubLocalization;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

import java.util.ArrayList;
import java.util.List;

public class MetaBlockWood extends Block implements ISubLocalization {

    @SideOnly(Side.CLIENT)
    private ArrayList<IIcon> textures;
    @SideOnly(Side.CLIENT)
    private ArrayList<IIcon> texturesTB;
    private String[] subNames;

    public MetaBlockWood(String name, String[] subNames) {
        super(Material.wood);
        this.setHardness(0.5f);
        this.setResistance(2.5f);
        this.setStepSound(Block.soundTypeWood);
        this.setBlockName(name);
        this.subNames = subNames;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public String getUnlocalizedName(String baseName, ItemStack itemStack) {
        int meta = itemStack.getItemDamage();
        return baseName + "." + subNames[meta];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < subNames.length; ++i)
            list.add(new ItemStack(block, 1, i));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        textures = new ArrayList<IIcon>();
        texturesTB = new ArrayList<IIcon>();

        for (int i = 0; i < subNames.length; i++) {
            textures.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[i]));
            if (i <= 2)
                texturesTB.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[i] + "_topBot"));
        }

        /*for (String subName : subNames) {
            textures.add(iconRegister.registerIcon(References.Texture_Prefix + subName));
            textures.add(iconRegister.registerIcon(References.Texture_Prefix + subName + "_topBot"));
        }*/

		/*textures[subNames.length] = iconRegister.registerIcon(References.Texture_Prefix+subNames[0]+"_topBot");
        textures[subNames.length+1] = iconRegister.registerIcon(References.Texture_Prefix+subNames[1]+"_topBot");
		textures[subNames.length+2] = iconRegister.registerIcon(References.Texture_Prefix+subNames[2]+"_topBot");*/
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if ((side == 0 || side == 1))
            if (meta != 3 && meta != 4)
                return texturesTB.get(meta);
            else
                return texturesTB.get(2);
        return textures.get(meta);
    }

    @Override
    public boolean isOpaqueCube() {
        return true;
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        switch (world.getBlockMetadata(x, y, z)) {
            default:
            case 0:
            case 1:
            case 2:
            case 3:
                return 0;
            case 4:
                return 9;
        }
    }
}
