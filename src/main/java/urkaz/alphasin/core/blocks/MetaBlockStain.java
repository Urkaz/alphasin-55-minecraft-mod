package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockBush;
import net.minecraft.block.IGrowable;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import urkaz.alphasin.core.blocks.items.ISubLocalization;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

import java.util.List;
import java.util.Random;

public class MetaBlockStain extends BlockBush implements ISubLocalization, IGrowable {

    @SideOnly(Side.CLIENT)
    private IIcon[] textures;
    private String[] subNames;

    public MetaBlockStain(String name, String[] subNames) {
        super(Material.plants);
        this.setStepSound(Block.soundTypeGrass);
        this.setBlockName(name);
        this.subNames = subNames;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public String getUnlocalizedName(String baseName, ItemStack itemStack) {
        int meta = itemStack.getItemDamage();
        return baseName + "." + subNames[meta];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < subNames.length; ++i)
            list.add(new ItemStack(block, 1, i));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        textures = new IIcon[subNames.length];

        for (int i = 0; i < subNames.length; ++i)
            textures[i] = iconRegister.registerIcon(References.Texture_Prefix + subNames[i]);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        return textures[meta];
    }

    @Override
    protected boolean canPlaceBlockOn(Block block) {
        return true;
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, int x, int y, int z, int side) {

        final Block dirt = G55Blocks.dirtBlock;
        final Block grass = G55Blocks.grassBlock;
        final Block fb = G55Blocks.fakeBlock;
        final Block craft = G55Blocks.craftingPanelFull;

        if (world.getBlock(x, y - 1, z) == dirt || world.getBlock(x, y - 1, z) == grass) {
            return true;
        }
        if (world.getBlock(x, y - 1, z) == craft || world.getBlock(x, y - 1, z) == fb ||
                world.getBlock(x, y - 2, z) == craft || world.getBlock(x, y - 2, z) == fb) {
            return true;
        }

        return false;
    }

    //canBonemeal(worldProvider, posX, posY, posZ, defaultValue)
    @Override
    public boolean func_149851_a(World world, int x, int y, int z, boolean defaultValue) {
        return world.getBlockMetadata(x, y, z) == 0;
    }

    //doBonemeal(worldProvider, randomProvider, posX, posY, posZ)
    @Override
    public boolean func_149852_a(World world, Random random, int x, int y, int z) {
        return true;
    }

    //doGrowthTick(worldProvider, randomProvider, posX, posY, posZ)
    @Override
    public void func_149853_b(World world, Random random, int x, int y, int z) {
        incrementGrowStage(world, random, x, y, z);
    }

    private void incrementGrowStage(World world, Random random, int x, int y, int z) {
        Block block = world.getBlock(x, y - 1, z);
        int growStage = world.getBlockMetadata(x, y, z);

        if (growStage == 0) {
            int blockMeta = world.getBlockMetadata(x, y - 1, z);
            if (block == G55Blocks.dirtBlock || block == G55Blocks.grassBlock) {
                world.setBlockMetadataWithNotify(x, y, z, blockMeta + 1, 1);
            } /*else if {
                //Desierto (Amarillos)
            }*/
        }
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random random) {
        super.updateTick(world, x, y, z, random);

        incrementGrowStage(world, random, x, y, z);
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        if (world.getBlockMetadata(x, y, z) > 0) {
            int random = world.rand.nextInt() * (100 + 1);
            if (random > 95) {
                EntityItem itemEntity = new EntityItem(world, x, y, z, new ItemStack(G55Blocks.stainBlock, 1, 0));
                world.spawnEntityInWorld(itemEntity);
            }
        }
    }

    public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor) {
        if (world.getBlock(x, y - 1, z) instanceof BlockAir) {
            world.func_147480_a(x, y, z, true);
        }
    }
}
