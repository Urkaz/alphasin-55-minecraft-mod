package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.blocks.items.ISubLocalization;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.entity.charges.EntityCharge;
import urkaz.alphasin.core.util.ChargeType;
import urkaz.alphasin.core.util.References;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MetaBlockCitronox extends Block implements ISubLocalization {

    @SideOnly(Side.CLIENT)
    private ArrayList<IIcon> textures;
    private String[] subNames;

    public MetaBlockCitronox(String name, String[] subNames) {
        super(Material.cactus);
        this.setHardness(0.5f);
        this.setResistance(2.5f);
        this.setStepSound(Block.soundTypeStone);
        this.setBlockName(name);
        this.subNames = subNames;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public String getUnlocalizedName(String baseName, ItemStack itemStack) {
        return baseName + "." + subNames[itemStack.getItemDamage()];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < subNames.length; ++i)
            list.add(new ItemStack(block, 1, i));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {

        textures = new ArrayList<IIcon>();

        textures.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[0] + "_top"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[0] + "_bot"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[0]));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[1] + "_top"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[1] + "_bot"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + subNames[1]));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {

        if ((meta == 0 || meta == 1)) {
            if (side == 0)
                return textures.get(meta * 3 + 1);
            else if (side == 1)
                return textures.get(meta * 3);
            else
                return textures.get(meta * 3 + 2);
        }

        return textures.get(meta);
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, int x, int y, int z, int side) {
        return !world.getBlock(x, y + 1, z).isAir(world, x, y + 1, z);
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        if (world.getBlockMetadata(x, y, z) == 0)
            return 8;
        return 0;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        super.breakBlock(world, x, y, z, block, meta);

        if (meta == 1)
            EntityCharge.spawnChargesInWorld(world, x + 0.5F, y, z + 0.5F, ChargeType.CITRONOX, 3);
    }

    @Override
    public Item getItemDropped(int meta, Random random, int fortune) {
        if (meta == 0)
            return Item.getItemFromBlock(G55Blocks.citronoxBlock);
        return null;
    }

    @Override
    public int getRenderType() {
        return Galaxy55Core.proxy.getBlockRender(this);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
        if (world.getBlockMetadata(x, y, z) == 0)
            setBlockBounds(0.25f, 0.5f, 0.25f, 0.75f, 1f, 0.75f);
        else
            setBlockBounds(0.25f, 0f, 0.25f, 0.75f, 0.5f, 0.75f);
    }

    public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor) {
        if (world.getBlock(x, y + 1, z) instanceof BlockAir) {
            world.func_147480_a(x, y, z, true);
        }
    }
}
