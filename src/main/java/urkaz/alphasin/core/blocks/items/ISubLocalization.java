package urkaz.alphasin.core.blocks.items;

import net.minecraft.item.ItemStack;

public interface ISubLocalization {
    public String getUnlocalizedName(String baseName, ItemStack itemStack);
}
