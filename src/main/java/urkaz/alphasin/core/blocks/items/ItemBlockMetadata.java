package urkaz.alphasin.core.blocks.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlockWithMetadata;
import net.minecraft.item.ItemStack;

public class ItemBlockMetadata extends ItemBlockWithMetadata {

    private Block block;

    public ItemBlockMetadata(Block block) {
        super(block, block);

        this.block = block;
    }

    @Override
    public String getUnlocalizedName(ItemStack itemStack) {
        if (block instanceof ISubLocalization)
            return ((ISubLocalization) block).getUnlocalizedName(super.getUnlocalizedName(itemStack), itemStack);
        else
            return super.getUnlocalizedName(itemStack);
    }
}