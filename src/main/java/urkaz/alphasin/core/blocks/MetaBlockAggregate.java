package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import urkaz.alphasin.core.blocks.items.ISubLocalization;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

import java.util.List;

public class MetaBlockAggregate extends Block implements ISubLocalization {

    @SideOnly(Side.CLIENT)
    private IIcon[] textures;
    private String[] subNames;

    public MetaBlockAggregate(String name, String[] subNames) {
        super(Material.rock);
        this.setHardness(2f);
        this.setResistance(30f);
        this.setStepSound(Block.soundTypeStone);
        this.setBlockName(name);
        this.subNames = subNames;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public String getUnlocalizedName(String baseName, ItemStack itemStack) {
        int meta = itemStack.getItemDamage();
        return baseName + "." + subNames[meta];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < subNames.length; ++i)
            list.add(new ItemStack(block, 1, i));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        textures = new IIcon[subNames.length];
        for (int i = 0; i < subNames.length; ++i)
            textures[i] = iconRegister.registerIcon(References.Texture_Prefix + subNames[i]);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        return textures[meta];
    }
}
