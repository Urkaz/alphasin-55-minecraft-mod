package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import urkaz.alphasin.core.tile.IMultiBlock;
import urkaz.alphasin.core.tile.TileEntityCraftingPanel;
import urkaz.alphasin.core.util.References;

import java.util.Random;

public class BlockCraftingPanelFull extends BlockTile {

    public BlockCraftingPanelFull(String assetName) {
        super(Material.iron);
        this.setHardness(1.0F);
        this.setResistance(10.0F);
        this.setStepSound(Block.soundTypeMetal);
        this.setBlockName(assetName);
    }

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        this.blockIcon = iconRegister.registerIcon(References.Texture_Prefix + "ensamb_top");
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public int quantityDropped(Random random) {
        return 4;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        final TileEntity tile = world.getTileEntity(x, y, z);

        if (tile instanceof IMultiBlock)
            ((IMultiBlock) tile).onDestroy(tile);

        super.breakBlock(world, x, y, z, block, meta);
    }

    @Override
    public Item getItemDropped(int meta, Random random, int fortune) {
        return Item.getItemFromBlock(G55Blocks.craftingPanel);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return G55Blocks.craftingPanel.getIcon(side, 0);
    }

    @Override
    public boolean hasTileEntity(int metadata) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, int metadata) {
        return new TileEntityCraftingPanel();
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer entityPlayer) {
        int metadata = world.getBlockMetadata(x, y, z);
        return new ItemStack(Item.getItemFromBlock(G55Blocks.craftingPanel), 1, metadata);
    }
}
