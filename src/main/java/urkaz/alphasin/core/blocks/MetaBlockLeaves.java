package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import urkaz.alphasin.core.blocks.items.ISubLocalization;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

import java.util.ArrayList;
import java.util.List;

public class MetaBlockLeaves extends Block implements ISubLocalization {

    @SideOnly(Side.CLIENT)
    private ArrayList<IIcon> textures;
    @SideOnly(Side.CLIENT)
    private IIcon pineTop, pineBottom, pineSnowTop;
    private String[] subNames;

    public MetaBlockLeaves(String name, String[] subNames) {
        super(Material.leaves);
        this.setHardness(0.5f);
        this.setResistance(2.5f);
        this.setStepSound(Block.soundTypeGrass);
        this.setBlockName(name);
        this.subNames = subNames;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public String getUnlocalizedName(String baseName, ItemStack itemStack) {
        int meta = itemStack.getItemDamage();
        return baseName + "." + subNames[meta];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < subNames.length; ++i)
            list.add(new ItemStack(block, 1, i));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {

        textures = new ArrayList<IIcon>();

        for (String subName : subNames)
            textures.add(iconRegister.registerIcon(References.Texture_Prefix + subName));

        pineBottom = iconRegister.registerIcon(References.Texture_Prefix + subNames[subNames.length - 2] + "_bot");
        pineTop = iconRegister.registerIcon(References.Texture_Prefix + subNames[subNames.length - 2] + "_top");
        pineSnowTop = iconRegister.registerIcon(References.Texture_Prefix + subNames[subNames.length - 1] + "_top");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {

        if ((meta == 8 || meta == 9) && side == 0)
            return pineBottom;
        else if (meta == 8 && side == 1)
            return pineTop;
        else if (meta == 9 && side == 1)
            return pineSnowTop;

        return textures.get(meta);
    }

    @Override
    public boolean isLeaves(IBlockAccess world, int x, int y, int z) {
        return true;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        switch (world.getBlockMetadata(x, y, z)) {
            default:
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 8:
            case 9:
                return 0;
            case 5:
            case 7:
                return 8;
        }
    }
}
