package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.tile.TileEntityCraftingPanelSingle;
import urkaz.alphasin.core.util.References;

public class BlockCraftingPanel extends BlockTile {

    @SideOnly(Side.CLIENT)
    private IIcon[] icons;

    public BlockCraftingPanel(String assetName) {
        super(Material.iron);
        this.setHardness(1.0F);
        this.setResistance(10.0F);
        this.setStepSound(Block.soundTypeMetal);
        this.setBlockName(assetName);
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        icons = new IIcon[2];

        this.icons[0] = iconRegister.registerIcon(References.Texture_Prefix + "ensamb_side");
        this.icons[1] = iconRegister.registerIcon(References.Texture_Prefix + "ensamb_top");
        this.blockIcon = iconRegister.registerIcon(References.Texture_Prefix + "ensamb_top");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if (side == 0)
            return G55Blocks.blindage.getIcon(side, meta);
        else if (side == 1)
            return icons[1];
        else
            return icons[0];
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, int x, int y, int z, int side) {
        final Block cp = G55Blocks.craftingPanel;
        final Block cpf = G55Blocks.craftingPanelFull;
        final Block fb = G55Blocks.fakeBlock;

        for (int i = 1; i <= 2; i++) {
            if (world.getBlock(x, y - i, z) == fb && world.getBlockMetadata(x, y - i, z) == 0 ||
                    world.getBlock(x, y - i, z) == cp || world.getBlock(x, y - i, z) == cpf)
                return false;
        }

        if (!(world.getBlock(x, y + 1, z) instanceof BlockAir && world.getBlock(x, y + 2, z) instanceof BlockAir))
            return false;

        return true;
    }

    @Override
    public boolean isOpaqueCube() {
        return true;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return null;
    }

    @Override
    public TileEntity createTileEntity(World world, int meta) {
        if (world.isRemote)
            return null;

        return new TileEntityCraftingPanelSingle();
    }
}
