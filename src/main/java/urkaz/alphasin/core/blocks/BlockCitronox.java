package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.entity.charges.EntityCharge;
import urkaz.alphasin.core.util.ChargeType;
import urkaz.alphasin.core.util.References;

import java.util.ArrayList;
import java.util.Random;

public class BlockCitronox extends Block {

    @SideOnly(Side.CLIENT)
    private ArrayList<IIcon> textures;

    protected BlockCitronox(String assetName) {
        super(Material.cactus);
        this.setHardness(0.5f);
        this.setResistance(2.5f);
        this.setStepSound(Block.soundTypeStone);
        this.setBlockName(assetName);
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {

        textures = new ArrayList<>();

        textures.add(iconRegister.registerIcon(References.Texture_Prefix + "citronox_top"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + "citronox_bot"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + "citronox"));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if (side == 0)
            return textures.get(meta * 3 + 1);
        else if (side == 1)
            return textures.get(meta * 3);
        else
            return textures.get(meta * 3 + 2);
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, int x, int y, int z, int side) {
        return !world.getBlock(x, y + 1, z).isAir(world, x, y + 1, z);
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        return 8;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        super.breakBlock(world, x, y, z, block, meta);
    }

    @Override
    public Item getItemDropped(int meta, Random random, int fortune) {
        return Item.getItemFromBlock(G55Blocks.citronoxBlock);
    }

    @Override
    public int getRenderType() {
        return Galaxy55Core.proxy.getBlockRender(this);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
        setBlockBounds(0.25f, 0.5f, 0.25f, 0.75f, 1f, 0.75f);
    }

    public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor) {
        if (world.getBlock(x, y + 1, z) instanceof BlockAir) {
            world.func_147480_a(x, y, z, true);
        }
    }
}
