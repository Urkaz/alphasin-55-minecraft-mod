package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

public class BlockWoodPlank extends Block {

    @SideOnly(Side.CLIENT)
    private IIcon[] icons;

    protected BlockWoodPlank(String assetName) {
        super(Material.wood);
        this.setHardness(5F);
        this.setStepSound(Block.soundTypeWood);
        this.setBlockName(assetName);
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        icons = new IIcon[2];

        this.icons[0] = iconRegister.registerIcon(References.Texture_Prefix + "woodPlank");
        this.icons[1] = iconRegister.registerIcon(References.Texture_Prefix + "woodPlank_topBot");
        this.blockIcon = iconRegister.registerIcon(References.Texture_Prefix + "woodPlank_topBot");
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if (side == 0 || side == 1)
            return icons[1];
        else
            return icons[0];
    }
}
