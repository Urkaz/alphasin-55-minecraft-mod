package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import urkaz.alphasin.core.blocks.items.ISubLocalization;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

import java.util.List;
import java.util.Random;

public class MetaBlockGrass extends Block implements ISubLocalization {

    @SideOnly(Side.CLIENT)
    private IIcon[] textures;
    private String[] subNames;

    public MetaBlockGrass(String name, String[] subNames) {
        super(Material.grass);
        this.setHardness(0.6f);
        this.setResistance(3f);
        this.setStepSound(Block.soundTypeGrass);
        this.setTickRandomly(true);
        this.setBlockName(name);
        this.subNames = subNames;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    @Override
    public String getUnlocalizedName(String baseName, ItemStack itemStack) {
        return baseName + "." + subNames[itemStack.getItemDamage()];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void getSubBlocks(Item block, CreativeTabs creativeTabs, List list) {
        for (int i = 0; i < subNames.length; ++i) {
            list.add(new ItemStack(block, 1, i));
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        textures = new IIcon[subNames.length * 2];

        for (int i = 0; i < subNames.length; ++i) {
            textures[i * 2] = iconRegister.registerIcon(References.Texture_Prefix + subNames[i] + "_top");
            textures[i * 2 + 1] = iconRegister.registerIcon(References.Texture_Prefix + subNames[i] + "_side");
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if (side == 0)
            return G55Blocks.dirtBlock.getIcon(side, meta);
        else if (side == 1)
            return textures[meta * 2];
        else
            return textures[meta * 2 + 1];
    }

    @Override
    public Item getItemDropped(int metadata, Random random, int fortune) {
        return G55Blocks.dirtBlock.getItemDropped(metadata, random, fortune);
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random random) {
        if (!world.isRemote) {
            if (world.getBlockLightValue(x, y + 1, z) < 4 && world.getBlockLightOpacity(x, y + 1, z) > 2) {
                world.setBlock(x, y, z, G55Blocks.dirtBlock, world.getBlockMetadata(x, y, z), 2);
            } else if (world.getBlockLightValue(x, y + 1, z) >= 9) {
                for (int l = 0; l < 4; ++l) {
                    int randX = x + random.nextInt(3) - 1;
                    int randY = y + random.nextInt(5) - 3;
                    int randZ = z + random.nextInt(3) - 1;

                    if (world.getBlockLightValue(randX, randY + 1, randZ) >= 4 && world.getBlockLightOpacity(randX, randY + 1, randZ) <= 2) {
                        if (world.getBlock(randX, randY, randZ) == G55Blocks.dirtBlock) {
                            int dirtMeta = world.getBlockMetadata(randX, randY, randZ);

                            world.setBlock(randX, randY, randZ, G55Blocks.grassBlock, dirtMeta, 2);
                        }
                    }
                }
            }
        }
    }

    @Override
    public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer entityPlayer) {
        int metadata = world.getBlockMetadata(x, y, z);
        return new ItemStack(Item.getItemFromBlock(G55Blocks.dirtBlock), 1, metadata);
    }
}
