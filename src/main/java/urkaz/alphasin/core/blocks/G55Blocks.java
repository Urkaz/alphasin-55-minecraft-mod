package urkaz.alphasin.core.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import urkaz.alphasin.core.blocks.items.ItemBlockMetadata;
import urkaz.alphasin.core.util.Name;

public class G55Blocks {

    //Blocks
    public static Block blindage;
    public static Block craftingPanel;
    public static Block craftingPanelFull;
    public static Block fakeBlock;
    public static Block synthesizer;
    public static Block mineralIron;
    public static Block mineralAluminium;
    public static Block woodPlank;
    public static Block citronoxBlock;
    public static Block citronoxBlockOld;

    //Metadata Blocks
    public static Block dirtBlock;
    public static Block grassBlock;
    public static Block rockBlock;
    public static Block minIngotBlock;
    public static Block leavesBlock;
    public static Block aggregateBlock;
    public static Block woodBlock;
    public static Block stainBlock;

    public static void registerBlocks() {

        dirtBlock = registerMetadataBlock(new MetaBlockDirt(Name.groupDirt_name, Name.mBlockDirt_subs));
        grassBlock = registerMetadataBlock(new MetaBlockGrass(Name.groupGrass_name, Name.mBlockGrass_subs));
        rockBlock = registerMetadataBlock(new MetaBlockRock(Name.groupRock_name, Name.mBlockRock_subs));
        aggregateBlock = registerMetadataBlock(new MetaBlockAggregate(Name.groupAggregate_name, Name.mBlockAggregate_subs));
        woodBlock = registerMetadataBlock(new MetaBlockWood(Name.groupWood_name, Name.mBlockWood_subs));
        leavesBlock = registerMetadataBlock(new MetaBlockLeaves(Name.groupLeaves_name, Name.mBlockLeaves_subs));
        stainBlock = registerMetadataBlock(new MetaBlockStain(Name.groupStain_name, Name.mBlockStain_subs));
        mineralAluminium = registerBlock(new BlockMineral(Name.blockMinAluminium_name));
        mineralIron = registerBlock(new BlockMineral(Name.blockMinIron_name));
        minIngotBlock = registerMetadataBlock(new MetaBlockIngot(Name.groupMineralItem_name, Name.mBlockMineral_subs));
        blindage = registerBlock(new BlockBlindage(Name.blockBlindage_name));
        woodPlank = registerBlock(new BlockWoodPlank(Name.blockWoodPlank_name));
        citronoxBlock = registerBlock(new BlockCitronox(Name.blockCitronox_name));
        citronoxBlockOld = registerBlock(new BlockCitronoxOld(Name.blockCitronoxOld_name));

        craftingPanel = registerBlock(new BlockCraftingPanel(Name.blockCraftingPanel_name));
        synthesizer = registerBlock(new BlockSynthesizer(Name.blockSynthesizer_name));

        craftingPanelFull = registerBlock(new BlockCraftingPanelFull(Name.blockCraftingPanel_name + "Full"));
        fakeBlock = registerBlock(new BlockMulti(Name.blockFakeBlock_name));


        //Register ores
        OreDictionary.registerOre("oreAluminum", new ItemStack(mineralAluminium, 1, 0));
        OreDictionary.registerOre("oreAluminium", new ItemStack(mineralAluminium, 1, 0));

        //OreDictionary.registerOre("ingotAluminum", new ItemStack(minIngotBlock, 1, 0));
        //OreDictionary.registerOre("ingotAluminium", new ItemStack(minIngotBlock, 1, 0));

        OreDictionary.registerOre("oreIron", new ItemStack(mineralIron, 1, 0));

        //OreDictionary.registerOre("ingotIron", new ItemStack(minIngotBlock, 1, 1));
    }

    public static Block registerMetadataBlock(Block block) {
        registerBlock(block, ItemBlockMetadata.class);
        return block;
    }

    public static Block registerBlock(Block block, Class<? extends ItemBlock> itemBlockClass) {
        GameRegistry.registerBlock(block, itemBlockClass, block.getUnlocalizedName().replace("tile.", ""));
        return block;
    }

    public static Block registerBlock(Block block) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
        return block;
    }
}
