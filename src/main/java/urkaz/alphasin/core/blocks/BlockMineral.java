package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.Name;
import urkaz.alphasin.core.util.References;

public class BlockMineral extends Block {

    @SideOnly(Side.CLIENT)
    private IIcon[] icons;
    private String name;

    public BlockMineral(String assetName) {
        super(Material.rock);
        this.setHardness(2f);
        this.setResistance(30f);
        this.setStepSound(Block.soundTypeStone);
        this.setBlockName(Name.groupMineral_name + "." + assetName);
        name = assetName;
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        icons = new IIcon[1];

        this.icons[0] = iconRegister.registerIcon(References.Texture_Prefix + "mineral_" + name);
        this.blockIcon = iconRegister.registerIcon(References.Texture_Prefix + "mineral_" + name);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        return icons[meta];
    }
}
