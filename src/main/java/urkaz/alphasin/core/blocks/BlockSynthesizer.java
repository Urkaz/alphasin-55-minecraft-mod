package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.tile.TileEntitySynthesizer;
import urkaz.alphasin.core.util.References;

public class BlockSynthesizer extends BlockTile {

    @SideOnly(Side.CLIENT)
    private IIcon[] icons;

    public BlockSynthesizer(String assetName) {
        super(Material.glass);
        this.setHardness(5F);
        this.setStepSound(Block.soundTypeMetal);
        this.setBlockName(assetName);
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {
        icons = new IIcon[2];

        icons[0] = iconRegister.registerIcon(References.Texture_Prefix + "transm_side");
        icons[1] = iconRegister.registerIcon(References.Texture_Prefix + "transm_topBot");
        this.blockIcon = iconRegister.registerIcon(References.Texture_Prefix + "transm_side");
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean canPlaceBlockOnSide(World world, int x, int y, int z, int meta) {
        final Block cp = G55Blocks.craftingPanel;
        final Block cpf = G55Blocks.craftingPanelFull;
        final Block fb = G55Blocks.fakeBlock;
        final Block sy = G55Blocks.synthesizer;

        boolean isOnSides = false;
        boolean topLocked = false;

        for (int i = 1; i <= 2; i++) {
            //Only on the sides of the crafting panel
            //west || north || east || south
            if (((world.getBlock(x - 1, y - i, z) == fb) && (world.getBlockMetadata(x - 1, y - i, z) == 0)) ||
                    ((world.getBlock(x, y - i, z - 1) == fb) && (world.getBlockMetadata(x, y - i, z - 1) == 0)) ||
                    (((world.getBlock(x + 1, y - i, z) == fb) && (world.getBlockMetadata(x + 1, y - i, z) == 0)) || (world.getBlock(x + 1, y - i, z) == cpf)) ||
                    (((world.getBlock(x, y - i, z + 1) == fb) && (world.getBlockMetadata(x, y - i, z + 1) == 0)) || (world.getBlock(x, y - i, z + 1) == cpf))) {
                isOnSides = true;
                break;
            }
        }

        for (int i = 1; i <= 2; i++) {
            //And not above it
            if (world.getBlock(x, y - i, z) == fb && world.getBlockMetadata(x, y - i, z) == 0) {
                topLocked = true;
                break;
            } else if (world.getBlock(x, y - i, z) == cp || world.getBlock(x, y - i, z) == cpf) {
                topLocked = true;
                break;
            }
        }
        return isOnSides && !topLocked;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer entityPlayer, int side,
                                    float hitX, float hitY, float hitZ) {

        final TileEntitySynthesizer tile = (TileEntitySynthesizer) world.getTileEntity(x, y, z);

        if (tile != null)
            tile.onActivated(entityPlayer, side);

        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, int meta) {
        return new TileEntitySynthesizer();
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if (side == 0 || side == 1)
            return icons[1];
        else
            return icons[0];
    }
}
