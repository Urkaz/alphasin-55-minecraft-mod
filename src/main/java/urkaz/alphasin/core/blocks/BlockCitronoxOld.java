package urkaz.alphasin.core.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.entity.charges.EntityCharge;
import urkaz.alphasin.core.util.ChargeType;
import urkaz.alphasin.core.util.References;

import java.util.ArrayList;
import java.util.Random;

public class BlockCitronoxOld extends Block {

    @SideOnly(Side.CLIENT)
    private ArrayList<IIcon> textures;

    protected BlockCitronoxOld(String assetName) {
        super(Material.cactus);
        this.setHardness(0.5f);
        this.setResistance(2.5f);
        this.setStepSound(Block.soundTypeStone);
        this.setBlockName(assetName);
    }

    @Override
    public CreativeTabs getCreativeTabToDisplayOn() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {

        textures = new ArrayList<>();

        textures.add(iconRegister.registerIcon(References.Texture_Prefix + "oldCitronox_top"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + "oldCitronox_bot"));
        textures.add(iconRegister.registerIcon(References.Texture_Prefix + "oldCitronox"));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int meta) {
        if (side == 0)
            return textures.get(meta * 3 + 1);
        else if (side == 1)
            return textures.get(meta * 3);
        else
            return textures.get(meta * 3 + 2);
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        super.breakBlock(world, x, y, z, block, meta);
        EntityCharge.spawnChargesInWorld(world, x + 0.5F, y, z + 0.5F, ChargeType.CITRONOX, 3);
    }

    @Override
    public Item getItemDropped(int meta, Random random, int fortune) {
        return null;
    }

    @Override
    public int getRenderType() {
        return Galaxy55Core.proxy.getBlockRender(this);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
        setBlockBounds(0.25f, 0f, 0.25f, 0.75f, 0.5f, 0.75f);
    }
}
