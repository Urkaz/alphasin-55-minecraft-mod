package urkaz.alphasin.core.api;

import net.minecraft.item.ItemStack;
import urkaz.alphasin.core.crafting.G55CraftingManager;

public class G55Registry {

    /*
    Top  1,2
         3,4

    Bot  5,6
         7,8
     */

    public static void addRecipe(ItemStack output, Object... input) {
        G55CraftingManager.getInstance().addRecipe(output, input);
    }
}
