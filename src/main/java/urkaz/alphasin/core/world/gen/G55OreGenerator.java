package urkaz.alphasin.core.world.gen;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import urkaz.alphasin.core.blocks.G55Blocks;

import java.util.Random;

public class G55OreGenerator implements IWorldGenerator {

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world,
                         IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        switch (world.provider.dimensionId) {
            case -1:
                //nether...
                break;
            case 0:
                generateSurface(world, random, chunkX * 16, chunkZ * 16);
                break;
            case 1:
                //end...
                break;
            default:
                break;
        }
    }

    public void generateSurface(World world, Random random, int x, int z) {
        this.addOreSpawn(G55Blocks.mineralAluminium, world, random, x, z, 16, 16, 15, 15, 0, 256);
        this.addOreSpawn(G55Blocks.mineralIron, world, random, x, z, 16, 16, 15, 15, 0, 256);
    }

	/*public void generateNether(World world, Random random, int x, int z)
    {

	}*/

    public void addOreSpawn(Block block, World world, Random random, int blockXPos, int blockZPos,
                            int maxX, int maxZ, int maxVeinSize, int chancesToSpawn, int minY, int maxY) {
        for (int i = 0; i < chancesToSpawn; i++) {
            int posX = blockXPos + random.nextInt(maxX);
            int posY = minY + random.nextInt(maxY - minY);
            int posZ = blockZPos + random.nextInt(maxZ);
            new WorldGenMinable(block, maxVeinSize).generate(world, random, posX, posY, posZ);
        }
    }
}
