package urkaz.alphasin.core.client.render.block;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.opengl.GL11;

public class BlockRendererIngot implements ISimpleBlockRenderingHandler {

    final int renderID;

    public BlockRendererIngot(int id) {
        this.renderID = id;
    }

    public static void renderInvNormalBlock(RenderBlocks renderer, Block block, int meta) {
        final Tessellator tessellator = Tessellator.instance;
        GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        renderer.setRenderBounds(0.25f, 0f, 0.406f, 0.75f, 1f, 0.594f);
        renderer.uvRotateSouth = 1;
        renderer.uvRotateNorth = 1;

        tessellator.startDrawingQuads();

        tessellator.setNormal(0.0F, -1F, 0.0F);
        renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, meta));

        tessellator.setNormal(0.0F, 1F, 0.0F);
        renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, meta));

        tessellator.setNormal(1F, 0.0F, 0.0F);
        renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, meta));

        tessellator.setNormal(-1F, 0.0F, 0.0F);
        renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, meta));

        tessellator.setNormal(0.0F, 0.0F, -1F);
        renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, meta));

        tessellator.setNormal(0.0F, 0.0F, 1F);
        renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, meta));

        tessellator.draw();

        renderer.uvRotateSouth = 0;
        renderer.uvRotateNorth = 0;
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        this.renderBlockIngot(renderer, block, world, x, y, z);
        return true;
    }

    @Override
    public boolean shouldRender3DInInventory(int modelId) {
        return true;
    }

    @Override
    public int getRenderId() {
        return this.renderID;
    }

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {
        renderInvNormalBlock(renderer, block, metadata);
    }

    public void renderBlockIngot(RenderBlocks renderer, Block block, IBlockAccess world, int x, int y, int z) {
        //Icon textures
        IIcon UDEW = block.getIcon(0, world.getBlockMetadata(x, y, z)); //Up, Down, East and West have the same texture.
        IIcon NS = block.getIcon(2, world.getBlockMetadata(x, y, z)); //North and south have the same texture.

        Tessellator tessellator = Tessellator.instance;
        // Brightness
        int lightValue = block.getMixedBrightnessForBlock(world, x, y, z);
        tessellator.setBrightness(lightValue);
        tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);

        double boundsMaxX = block.getBlockBoundsMaxX();
        double boundsMinX = block.getBlockBoundsMinX();
        double boundsMaxZ = block.getBlockBoundsMaxZ();
        double boundsMinZ = block.getBlockBoundsMinZ();

        //Down (0)
        tessellator.addVertexWithUV(x + boundsMaxX, y, z + boundsMinZ, UDEW.getInterpolatedU(8), UDEW.getInterpolatedV(boundsMinZ * 16));
        tessellator.addVertexWithUV(x + boundsMaxX, y, z + boundsMaxZ, UDEW.getInterpolatedU(8), UDEW.getInterpolatedV(boundsMaxZ * 16));
        tessellator.addVertexWithUV(x + boundsMinX, y, z + boundsMaxZ, UDEW.getMaxU(), UDEW.getInterpolatedV(boundsMaxZ * 16));
        tessellator.addVertexWithUV(x + boundsMinX, y, z + boundsMinZ, UDEW.getMaxU(), UDEW.getInterpolatedV(boundsMinZ * 16));

        //Top (1)
        tessellator.addVertexWithUV(x + boundsMinX, y + 1, z + boundsMinZ, UDEW.getInterpolatedU(8), UDEW.getInterpolatedV(boundsMaxZ * 16));
        tessellator.addVertexWithUV(x + boundsMinX, y + 1, z + boundsMaxZ, UDEW.getInterpolatedU(8), UDEW.getInterpolatedV(boundsMinZ * 16));
        tessellator.addVertexWithUV(x + boundsMaxX, y + 1, z + boundsMaxZ, UDEW.getMinU(), UDEW.getInterpolatedV(boundsMinZ * 16));
        tessellator.addVertexWithUV(x + boundsMaxX, y + 1, z + boundsMinZ, UDEW.getMinU(), UDEW.getInterpolatedV(boundsMaxZ * 16));

        //North (2)
        tessellator.addVertexWithUV(x + boundsMinX, y, z + boundsMinZ, NS.getInterpolatedU(boundsMaxX * 16), NS.getMaxV());
        tessellator.addVertexWithUV(x + boundsMinX, y + 1, z + boundsMinZ, NS.getInterpolatedU(boundsMaxX * 16), NS.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + 1, z + boundsMinZ, NS.getInterpolatedU(boundsMinX * 16), NS.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y, z + boundsMinZ, NS.getInterpolatedU(boundsMinX * 16), NS.getMaxV());

        //South (3)
        tessellator.addVertexWithUV(x + boundsMaxX, y, z + boundsMaxZ, NS.getInterpolatedU(boundsMaxX * 16), NS.getMaxV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + 1, z + boundsMaxZ, NS.getInterpolatedU(boundsMaxX * 16), NS.getMinV());
        tessellator.addVertexWithUV(x + boundsMinX, y + 1, z + boundsMaxZ, NS.getInterpolatedU(boundsMinX * 16), NS.getMinV());
        tessellator.addVertexWithUV(x + boundsMinX, y, z + boundsMaxZ, NS.getInterpolatedU(boundsMinX * 16), NS.getMaxV());

        //West (4)
        tessellator.addVertexWithUV(x + boundsMinX, y, z + boundsMaxZ, UDEW.getMaxU(), UDEW.getInterpolatedV(boundsMaxZ * 16));
        tessellator.addVertexWithUV(x + boundsMinX, y + 1, z + boundsMaxZ, UDEW.getMinU(), UDEW.getInterpolatedV(boundsMaxZ * 16));
        tessellator.addVertexWithUV(x + boundsMinX, y + 1, z + boundsMinZ, UDEW.getMinU(), UDEW.getInterpolatedV(boundsMinZ * 16));
        tessellator.addVertexWithUV(x + boundsMinX, y, z + boundsMinZ, UDEW.getMaxU(), UDEW.getInterpolatedV(boundsMinZ * 16));

        //East (5)
        tessellator.addVertexWithUV(x + boundsMaxX, y, z + boundsMinZ, UDEW.getMaxU(), UDEW.getInterpolatedV(boundsMaxZ * 16));
        tessellator.addVertexWithUV(x + boundsMaxX, y + 1, z + boundsMinZ, UDEW.getMinU(), UDEW.getInterpolatedV(boundsMaxZ * 16));
        tessellator.addVertexWithUV(x + boundsMaxX, y + 1, z + boundsMaxZ, UDEW.getMinU(), UDEW.getInterpolatedV(boundsMinZ * 16));
        tessellator.addVertexWithUV(x + boundsMaxX, y, z + boundsMaxZ, UDEW.getMaxU(), UDEW.getInterpolatedV(boundsMinZ * 16));
    }
}
