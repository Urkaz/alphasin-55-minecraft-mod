package urkaz.alphasin.core.client.render.tile;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.tileentity.TileEntity;
import org.lwjgl.opengl.GL11;
import urkaz.alphasin.core.tile.TileEntitySynthesizer;

public class TileEntityRendererSynthesizer extends TileEntitySpecialRenderer {

    private EntityItem entItem = null;

    @Override
    public void renderTileEntityAt(TileEntity entity, double x, double y, double z, float partialTickTime) {

        TileEntitySynthesizer tileSynth = (TileEntitySynthesizer) entity.getWorldObj().getTileEntity(entity.xCoord, entity.yCoord, entity.zCoord);

        if (tileSynth.getOutput() != null) {

            if (entItem == null || entItem.getEntityItem().getItem() != tileSynth.getOutput().getItem() || entItem.getEntityItem().getItemDamage() != tileSynth.getOutput().getItemDamage()) {
                entItem = new EntityItem(tileSynth.getWorldObj(), x, y, z, tileSynth.getOutput());
                entItem.getEntityItem().setItemDamage(tileSynth.getOutput().getItemDamage());
                this.entItem.hoverStart = 0.0F;
            }

            float ticks = (float) Minecraft.getMinecraft().renderViewEntity.ticksExisted + partialTickTime;

            GL11.glPushMatrix();
            GL11.glTranslatef((float) x + 0.5F, (float) (y + 0.2F + Math.sin(Math.toRadians(ticks % 360.0F) * 4) / 10), (float) z + 0.5F);
            GL11.glRotatef(ticks % 360.0F * 4, 0.0F, 1.0F, 0.0F);
            RenderItem.renderInFrame = true;
            RenderManager.instance.renderEntityWithPosYaw(this.entItem, 0.0D, 0.0D, 0.0D, 0.0F, 0.0F);
            RenderItem.renderInFrame = false;
            GL11.glPopMatrix();
        }
    }
}
