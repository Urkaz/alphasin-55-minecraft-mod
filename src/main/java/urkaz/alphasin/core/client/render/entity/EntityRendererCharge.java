package urkaz.alphasin.core.client.render.entity;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import urkaz.alphasin.core.entity.charges.EntityCharge;
import urkaz.alphasin.core.util.References;

public class EntityRendererCharge extends Render {

    @Override
    public void doRender(Entity entity, double x, double y, double z, float par5, float par6) {
        this.doRender((EntityCharge) entity, x, y, z, par5, par6);
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return getEntityTexture((EntityCharge) entity);
    }

    protected ResourceLocation getEntityTexture(EntityCharge entity) {
        return new ResourceLocation(References.Assets_Prefix, "textures/items/" + entity.getChargeType().getName() + ".png");
    }

    public void doRender(EntityCharge entity, double x, double y, double z, float par5, float par6) {

        GL11.glPushMatrix();
        GL11.glTranslatef((float) x, (float) y, (float) z);
        this.bindEntityTexture(entity);

        /*int brightnessForRender = entity.getBrightnessForRender(par6);
        int u = brightnessForRender % 65536;
        int v = brightnessForRender / 65536;
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) u / 1.0F, (float) v / 1.0F);*/

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glRotatef(180.0F - this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
        float scale = 0.3F;
        GL11.glScalef(scale, scale, scale);
        int zLevel = 0;
        Tessellator tessellator = Tessellator.instance;

        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 1.0F, 0.0F);
        tessellator.addVertexWithUV(0, 0, zLevel, 1, 1);
        tessellator.addVertexWithUV(1, 0, zLevel, 0, 1);
        tessellator.addVertexWithUV(1, 1, zLevel, 0, 0);
        tessellator.addVertexWithUV(0, 1, zLevel, 1, 0);
        tessellator.draw();

        GL11.glPopMatrix();
    }
}
