package urkaz.alphasin.core.client.render.block;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.opengl.GL11;

public class BlockRendererCitronox implements ISimpleBlockRenderingHandler {

    final int renderID;

    public BlockRendererCitronox(int id) {
        this.renderID = id;
    }

    public static void renderInvNormalBlock(RenderBlocks renderer, Block block, int meta) {
        final Tessellator tessellator = Tessellator.instance;
        GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        if (meta == 0)
            renderer.setRenderBounds(0.125f, 0.25f, 0.125f, 0.875f, 1f, 0.875f);
        else
            renderer.setRenderBounds(0.125f, 0f, 0.125f, 0.875f, 0.75f, 0.875f);

        tessellator.startDrawingQuads();

        tessellator.setNormal(0.0F, -1F, 0.0F);
        renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, meta));

        tessellator.setNormal(0.0F, 1F, 0.0F);
        renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, meta));

        tessellator.setNormal(1F, 0.0F, 0.0F);
        renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, meta));

        tessellator.setNormal(-1F, 0.0F, 0.0F);
        renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, meta));

        tessellator.setNormal(0.0F, 0.0F, -1F);
        renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, meta));

        tessellator.setNormal(0.0F, 0.0F, 1F);
        renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, meta));

        tessellator.draw();
    }

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
        renderInvNormalBlock(renderer, block, metadata);
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        this.renderBlockCitronox(renderer, block, world, x, y, z);
        return true;
    }

    @Override
    public boolean shouldRender3DInInventory(int modelId) {
        return true;
    }

    @Override
    public int getRenderId() {
        return renderID;
    }

    public void renderBlockCitronox(RenderBlocks renderer, Block block, IBlockAccess world, int x, int y, int z) {
        //Icon textures
        IIcon TOP = block.getIcon(1, world.getBlockMetadata(x, y, z));
        IIcon BOT = block.getIcon(0, world.getBlockMetadata(x, y, z));
        IIcon SIDE = block.getIcon(2, world.getBlockMetadata(x, y, z));

        Tessellator tessellator = Tessellator.instance;
        // Brightness
        int lightValue = block.getMixedBrightnessForBlock(world, x, y, z);
        tessellator.setBrightness(lightValue);
        tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);

        double boundsMaxX = block.getBlockBoundsMaxX();
        double boundsMinX = block.getBlockBoundsMinX();
        double boundsMaxY = block.getBlockBoundsMaxY();
        double boundsMinY = block.getBlockBoundsMinY();
        double boundsMaxZ = block.getBlockBoundsMaxZ();
        double boundsMinZ = block.getBlockBoundsMinZ();

        //Down (0)
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMinY, z + boundsMinZ, BOT.getMinU(), BOT.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMinY, z + boundsMaxZ, BOT.getMinU(), BOT.getMaxV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMinY, z + boundsMaxZ, BOT.getMaxU(), BOT.getMaxV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMinY, z + boundsMinZ, BOT.getMaxU(), BOT.getMinV());

        //Top (1)
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMaxY, z + boundsMinZ, TOP.getMaxU(), TOP.getMaxV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMaxY, z + boundsMaxZ, TOP.getMaxU(), TOP.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMaxY, z + boundsMaxZ, TOP.getMinU(), TOP.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMaxY, z + boundsMinZ, TOP.getMinU(), TOP.getMaxV());

        //North (2)
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMinY, z + boundsMinZ, SIDE.getMaxU(), SIDE.getMaxV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMaxY, z + boundsMinZ, SIDE.getMaxU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMaxY, z + boundsMinZ, SIDE.getMinU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMinY, z + boundsMinZ, SIDE.getMinU(), SIDE.getMaxV());

        //South (3)
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMinY, z + boundsMaxZ, SIDE.getMaxU(), SIDE.getMaxV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMaxY, z + boundsMaxZ, SIDE.getMaxU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMaxY, z + boundsMaxZ, SIDE.getMinU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMinY, z + boundsMaxZ, SIDE.getMinU(), SIDE.getMaxV());

        //West (4)
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMinY, z + boundsMaxZ, SIDE.getMaxU(), SIDE.getMaxV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMaxY, z + boundsMaxZ, SIDE.getMaxU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMaxY, z + boundsMinZ, SIDE.getMinU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMinX, y + boundsMinY, z + boundsMinZ, SIDE.getMinU(), SIDE.getMaxV());

        //East (5)
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMinY, z + boundsMinZ, SIDE.getMaxU(), SIDE.getMaxV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMaxY, z + boundsMinZ, SIDE.getMaxU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMaxY, z + boundsMaxZ, SIDE.getMinU(), SIDE.getMinV());
        tessellator.addVertexWithUV(x + boundsMaxX, y + boundsMinY, z + boundsMaxZ, SIDE.getMinU(), SIDE.getMaxV());
    }
}
