package urkaz.alphasin.core.client.gui;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.opengl.GL11;
import urkaz.alphasin.core.entity.player.ChargesProperties;
import urkaz.alphasin.core.util.ChargeType;
import urkaz.alphasin.core.util.References;

public class ChargesGui extends Gui {

    private static Minecraft mc = FMLClientHandler.instance().getClient();

    @SubscribeEvent
    public void onRenderGameOverlay(RenderGameOverlayEvent event) {
        if (event.isCancelable() || event.type != RenderGameOverlayEvent.ElementType.EXPERIENCE) {
            return;
        }

        ChargesGui.mc.entityRenderer.setupOverlayRendering();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        final Tessellator tessellator = Tessellator.instance;

        int size = 16;
        int posX = 5;
        int posY = event.resolution.getScaledHeight() - size - 5;
        ChargesProperties charges = ChargesProperties.get(ChargesGui.mc.thePlayer);
        ChargeType[] chargeTypes = ChargeType.values();

        for (ChargeType chargeType : chargeTypes) {

            ResourceLocation guiTexture = new ResourceLocation(References.Assets_Prefix, "textures/items/" + chargeType.getName() + ".png");
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(guiTexture);

            tessellator.startDrawingQuads();
            tessellator.addVertexWithUV(posX, posY + size, zLevel, 0, 1);
            tessellator.addVertexWithUV(posX + size, posY + size, zLevel, 1, 1);
            tessellator.addVertexWithUV(posX + size, posY, zLevel, 1, 0);
            tessellator.addVertexWithUV(posX, posY, zLevel, 0, 0);
            tessellator.draw();

            if (charges != null) {
                FontRenderer fontRender = ChargesGui.mc.fontRenderer;

                String text = Integer.toString(charges.getCharge(chargeType));
                int x = posX + size / 2 - 3;
                int y = posY - 2;
                int color = 0xFFFFFF;
                fontRender.drawStringWithShadow(text, x, y, color);
            }

            posX += 2 + size;
        }
    }
}
