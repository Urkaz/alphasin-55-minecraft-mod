package urkaz.alphasin.core.item;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import urkaz.alphasin.core.util.Name;

public class G55Items {

    public static Item itemCharge;
    public static Item itemMiningLaser;

    public static void registerItems() {
        itemCharge = registerItem(new ItemCharge(Name.groupCharges_name, Name.mItemCharges_subs));
        itemMiningLaser = registerItem(new ItemMiningLaser(Name.itemMiningLaser_name));
    }

    public static Item registerItem(Item item) {
        GameRegistry.registerItem(item, item.getUnlocalizedName());
        return item;
    }
}
