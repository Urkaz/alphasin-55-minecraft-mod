package urkaz.alphasin.core.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.References;

public class ItemMiningLaser extends Item {

    @SideOnly(Side.CLIENT)
    private IIcon texture;

    public ItemMiningLaser(String name) {
        super();
        this.setUnlocalizedName(name);
    }

    @Override
    public CreativeTabs getCreativeTab() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IIconRegister iconRegister) {
        texture = iconRegister.registerIcon(References.Texture_Prefix + "miningLaser");
    }

    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int meta) {
        return texture;
    }
}
