package urkaz.alphasin.core.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import urkaz.alphasin.core.creativetab.G55CreativeTab;
import urkaz.alphasin.core.util.ChargeType;
import urkaz.alphasin.core.util.References;

import java.util.List;

public class ItemCharge extends Item {

    @SideOnly(Side.CLIENT)
    private IIcon[] textures;
    private String[] subNames;

    public ItemCharge(String name, String[] subNames) {
        super();
        this.setHasSubtypes(true);
        this.setUnlocalizedName(name);
        this.subNames = subNames;
    }

    public static boolean isCharge(Item item) {
        return item instanceof ItemCharge;
    }

    public static ChargeType getChargeType(int meta) {
        switch (meta) {
            default:
            case 0:
                return ChargeType.CITRONOX;
            case 1:
                return ChargeType.ETERIUM;
            case 2:
                return ChargeType.PURPLE;
            case 3:
                return ChargeType.GEM;
        }
    }

    @Override
    public CreativeTabs getCreativeTab() {
        return G55CreativeTab.g55tab;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IIconRegister iconRegister) {
        textures = new IIcon[subNames.length];
        for (int i = 0; i < subNames.length; i++)
            textures[i] = iconRegister.registerIcon(References.Texture_Prefix + subNames[i]);
    }

    @Override
    public IIcon getIconFromDamage(int meta) {
        return textures[meta];
    }

    @Override
    @SuppressWarnings("unchecked")
    public void getSubItems(Item item, CreativeTabs tab, List list) {
        for (int i = 0; i < subNames.length; ++i)
            list.add(new ItemStack(item, 1, i));
    }

    @Override
    public String getUnlocalizedName(ItemStack itemStack) {
        int meta = itemStack.getItemDamage();
        return this.getUnlocalizedName() + "." + subNames[meta];
    }

    /*@Override
    public boolean onItemUse(ItemStack itemStack, EntityPlayer entityPlayer, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        MinecraftForge.EVENT_BUS.post(new PlayerPickupChargeEvent(entityPlayer, this.getChargeType(itemStack.getItemDamage())));
        return true;
    }*/
}
