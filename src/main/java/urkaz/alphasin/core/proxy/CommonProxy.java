package urkaz.alphasin.core.proxy;

import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;

public class CommonProxy {

    public void registerRenderers() {
        // Nothing here as the server doesn't render graphics or entities!
    }

    public void registerGUI() {
    }

    public void renderDestroyEffects(int x, int y, int z, Block block, int blockID) {
    }

    public int getBlockRender(Block blockID) {
        return -1;
    }

    public EntityPlayer getPlayerFromMessageContext(MessageContext context) {
        switch (context.side) {
            case CLIENT:
                assert false : "Message for CLIENT received on dedicated server";
            case SERVER:
                return context.getServerHandler().playerEntity;
            default:
                assert false : "Invalid side in TestMsgHandler: " + context.side;
        }
        return null;
    }

    public World getWorldFromMessageContext(MessageContext context) {
        switch (context.side) {
            case CLIENT:
                assert false : "Message for CLIENT received on dedicated server";
            case SERVER:
                EntityPlayer player = context.getServerHandler().playerEntity;
                int dimension = player.dimension;
                World world = DimensionManager.getWorld(dimension);
                return world;
            default:
                assert false : "Invalid side in TestMsgHandler: " + context.side;
        }
        return null;
    }
}
