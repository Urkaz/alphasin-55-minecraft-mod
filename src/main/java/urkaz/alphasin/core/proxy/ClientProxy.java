package urkaz.alphasin.core.proxy;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import urkaz.alphasin.core.blocks.G55Blocks;
import urkaz.alphasin.core.client.gui.ChargesGui;
import urkaz.alphasin.core.client.render.block.BlockRendererCitronox;
import urkaz.alphasin.core.client.render.block.BlockRendererIngot;
import urkaz.alphasin.core.client.render.entity.EntityRendererCharge;
import urkaz.alphasin.core.client.render.tile.TileEntityRendererSynthesizer;
import urkaz.alphasin.core.entity.charges.EntityChargeCitronox;
import urkaz.alphasin.core.entity.charges.EntityChargeEterium;
import urkaz.alphasin.core.entity.charges.EntityChargeGem;
import urkaz.alphasin.core.entity.charges.EntityChargePurple;
import urkaz.alphasin.core.tile.TileEntitySynthesizer;

public class ClientProxy extends CommonProxy {

    public static int renderIdAluminium;
    public static int renderIdCitronox;

    @Override
    public void renderDestroyEffects(int x, int y, int z, Block block, int blockID) {
        FMLClientHandler.instance().getClient().effectRenderer.addBlockDestroyEffects(x, y, z, block, blockID);
    }

    @Override
    public void registerRenderers() {
        ClientProxy.renderIdAluminium = RenderingRegistry.getNextAvailableRenderId();
        ClientProxy.renderIdCitronox = RenderingRegistry.getNextAvailableRenderId();

        RenderingRegistry.registerBlockHandler(new BlockRendererIngot(ClientProxy.renderIdAluminium));
        RenderingRegistry.registerBlockHandler(new BlockRendererCitronox(ClientProxy.renderIdCitronox));

        RenderingRegistry.registerEntityRenderingHandler(EntityChargeCitronox.class, new EntityRendererCharge());
        RenderingRegistry.registerEntityRenderingHandler(EntityChargeEterium.class, new EntityRendererCharge());
        RenderingRegistry.registerEntityRenderingHandler(EntityChargePurple.class, new EntityRendererCharge());
        RenderingRegistry.registerEntityRenderingHandler(EntityChargeGem.class, new EntityRendererCharge());

        ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySynthesizer.class, new TileEntityRendererSynthesizer());
    }

    @Override
    public void registerGUI() {
        MinecraftForge.EVENT_BUS.register(new ChargesGui());
    }

    @Override
    public int getBlockRender(Block blockID) {
        if (blockID == G55Blocks.minIngotBlock)
            return ClientProxy.renderIdAluminium;
        else if (blockID == G55Blocks.citronoxBlock || blockID == G55Blocks.citronoxBlockOld)
            return ClientProxy.renderIdCitronox;

        return -1;
    }

    @Override
    public EntityPlayer getPlayerFromMessageContext(MessageContext context) {
        switch (context.side) {
            case CLIENT:
                return Minecraft.getMinecraft().thePlayer;
            case SERVER:
                return context.getServerHandler().playerEntity;
            default:
                assert false : "Invalid side in TestMsgHandler: " + context.side;
        }
        return null;
    }

    @Override
    public World getWorldFromMessageContext(MessageContext context) {
        EntityPlayer player;
        int dimension;
        World world;
        switch (context.side) {
            case CLIENT:
                player = Minecraft.getMinecraft().thePlayer;
                dimension = player.dimension;
                world = DimensionManager.getWorld(dimension);
                return world;
            case SERVER:
                player = context.getServerHandler().playerEntity;
                dimension = player.dimension;
                world = DimensionManager.getWorld(dimension);
                return world;
            default:
                assert false : "Invalid side in TestMsgHandler: " + context.side;
        }
        return null;
    }
}
