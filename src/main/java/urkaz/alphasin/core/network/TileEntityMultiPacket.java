package urkaz.alphasin.core.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;
import micdoodle8.mods.galacticraft.api.vector.BlockVec3;
import net.minecraft.nbt.NBTTagCompound;

public class TileEntityMultiPacket implements IMessage {

    private BlockVec3 coordinate;
    private BlockVec3 mainBlock;

    public TileEntityMultiPacket() {
    }

    public TileEntityMultiPacket(BlockVec3 coordinate, BlockVec3 mainBlock) {
        this.coordinate = coordinate;
        this.mainBlock = mainBlock;
    }


    @Override
    public void fromBytes(ByteBuf buf) {
        NBTTagCompound nbtTag = ByteBufUtils.readTag(buf);
        coordinate = new BlockVec3(nbtTag.getCompoundTag("coordinate"));
        mainBlock = new BlockVec3(nbtTag.getCompoundTag("mainBlock"));
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NBTTagCompound nbtTag = new NBTTagCompound();
        nbtTag.setTag("coordinate", this.coordinate.writeToNBT(new NBTTagCompound()));
        nbtTag.setTag("mainBlock", this.mainBlock.writeToNBT(new NBTTagCompound()));
        ByteBufUtils.writeTag(buf, nbtTag);
    }

    public BlockVec3 getCoordinate() {
        return this.coordinate;
    }

    public BlockVec3 getMainBlock() {
        return this.mainBlock;
    }
}
