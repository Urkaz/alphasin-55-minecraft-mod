package urkaz.alphasin.core.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;

public class ChargesPacket implements IMessage {

    private int[] charges;

    public ChargesPacket() {
    }

    public ChargesPacket(int[] charges) {
        this.charges = charges;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        NBTTagCompound nbtTag = ByteBufUtils.readTag(buf);
        charges = nbtTag.getIntArray("charges");
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NBTTagCompound nbtTag = new NBTTagCompound();
        nbtTag.setIntArray("charges", charges);
        ByteBufUtils.writeTag(buf, nbtTag);
    }

    public int[] getCharges() {
        return charges;
    }
}
