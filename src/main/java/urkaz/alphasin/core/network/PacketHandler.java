package urkaz.alphasin.core.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import micdoodle8.mods.galacticraft.api.vector.BlockVec3;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.entity.player.ChargesProperties;
import urkaz.alphasin.core.tile.TileEntityMulti;

public class PacketHandler {

    public static class ChargesHandler implements IMessageHandler<ChargesPacket, IMessage> {

        @Override
        public IMessage onMessage(ChargesPacket message, MessageContext ctx) {
            EntityPlayer player = Galaxy55Core.proxy.getPlayerFromMessageContext(ctx);
            if (ctx.side == Side.CLIENT) {
                ChargesProperties prop = ChargesProperties.get(player);
                prop.setCharges(message.getCharges());
            }
            return null;
        }
    }

    public static class TileEntityMultiHandler implements IMessageHandler<TileEntityMultiPacket, IMessage> {

        @Override
        public IMessage onMessage(TileEntityMultiPacket message, MessageContext ctx) {
            World worldObj = Galaxy55Core.proxy.getWorldFromMessageContext(ctx);
            BlockVec3 c = message.getCoordinate();
            BlockVec3 mb = message.getMainBlock();

            TileEntityMulti tem = (TileEntityMulti) worldObj.getTileEntity(c.x, c.y, c.z);
            tem.mainBlockPosition = mb;

            return null;
        }
    }
}
