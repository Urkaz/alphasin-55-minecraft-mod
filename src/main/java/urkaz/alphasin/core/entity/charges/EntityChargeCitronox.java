package urkaz.alphasin.core.entity.charges;

import net.minecraft.world.World;

public class EntityChargeCitronox extends EntityCharge {

    public EntityChargeCitronox(World world) {
        super(world);
        super.setChargeMeta(0);
    }

    public EntityChargeCitronox(World world, double x, double y, double z) {
        super(world, x, y, z, 0);
    }
}
