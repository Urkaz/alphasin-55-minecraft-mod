package urkaz.alphasin.core.entity.charges;

import net.minecraft.world.World;

public class EntityChargeEterium extends EntityCharge {

    public EntityChargeEterium(World world) {
        super(world);
        super.setChargeMeta(1);
    }

    public EntityChargeEterium(World world, double x, double y, double z) {
        super(world, x, y, z, 1);
    }
}
