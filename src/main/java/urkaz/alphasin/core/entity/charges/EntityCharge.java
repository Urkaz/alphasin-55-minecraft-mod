package urkaz.alphasin.core.entity.charges;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import urkaz.alphasin.core.event.PlayerPickupChargeEvent;
import urkaz.alphasin.core.util.ChargeType;

public class EntityCharge extends Entity {

    private int chargeMeta = 0;

    public EntityCharge(World world) {
        super(world);
        this.setSize(0.5F, 0.5F);
        this.yOffset = this.height / 2.0F;
        this.rotationYaw = (float) (Math.random() * 360.0D);
        this.motionX = (double) ((float) (Math.random() * 0.20000000298023224D - 0.10000000149011612D) * 2.0F);
        this.motionY = (double) ((float) (Math.random() * 0.2D) * 2.0F);
        this.motionZ = (double) ((float) (Math.random() * 0.20000000298023224D - 0.10000000149011612D) * 2.0F);
    }

    public EntityCharge(World world, double x, double y, double z, int chargeMeta) {
        super(world);
        this.setSize(0.5F, 0.5F);
        this.yOffset = this.height / 2.0F;
        this.setPosition(x, y, z);
        this.rotationYaw = (float) (Math.random() * 360.0D);
        this.motionX = (double) ((float) (Math.random() * 0.20000000298023224D - 0.10000000149011612D) * 2.0F);
        this.motionY = (double) ((float) (Math.random() * 0.2D) * 2.0F);
        this.motionZ = (double) ((float) (Math.random() * 0.20000000298023224D - 0.10000000149011612D) * 2.0F);
        this.chargeMeta = chargeMeta;
    }

    public static void spawnChargesInWorld(World world, double x, double y, double z, ChargeType type, int amount) {
        for (int i = 0; i < amount; i++)
            spawnChargeInWorld(world, x, y, z, type);
    }

    public static void spawnChargesInWorldWithMotion(World world, double x, double y, double z, ChargeType type, int amount, float motionX, float motionY, float motionZ) {
        for (int i = 0; i < amount; i++)
            spawnChargeInWorldWithMotion(world, x, y, z, type, motionX, motionY, motionZ);
    }

    public static void spawnChargeInWorld(World world, double x, double y, double z, ChargeType type) {
        EntityCharge charge;
        switch (type) {
            default:
            case CITRONOX:
                charge = new EntityChargeCitronox(world, x, y, z);
                break;
            case ETERIUM:
                charge = new EntityChargeEterium(world, x, y, z);
                break;
            case PURPLE:
                charge = new EntityChargePurple(world, x, y, z);
                break;
            case GEM:
                charge = new EntityChargeGem(world, x, y, z);
                break;
        }
        charge.motionX = world.rand.nextFloat() * 0.05F;
        charge.motionY = 0;
        charge.motionZ = world.rand.nextFloat() * 0.05F;
        world.spawnEntityInWorld(charge);
    }

    public static void spawnChargeInWorldWithMotion(World world, double x, double y, double z, ChargeType type, float motionX, float motionY, float motionZ) {
        EntityCharge charge;
        switch (type) {
            default:
            case CITRONOX:
                charge = new EntityChargeCitronox(world, x, y, z);
                break;
            case ETERIUM:
                charge = new EntityChargeEterium(world, x, y, z);
                break;
            case PURPLE:
                charge = new EntityChargePurple(world, x, y, z);
                break;
            case GEM:
                charge = new EntityChargeGem(world, x, y, z);
                break;
        }
        charge.motionX = motionX + world.rand.nextFloat() * 0.05F;
        charge.motionY = motionY;
        charge.motionZ = motionZ + world.rand.nextFloat() * 0.05F;
        world.spawnEntityInWorld(charge);
    }

    public void setChargeMeta(int chargeMeta) {
        this.chargeMeta = chargeMeta;
    }

    protected boolean canTriggerWalking() {
        return false;
    }

    @Override
    protected void entityInit() {
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound nbtTag) {
        chargeMeta = nbtTag.getInteger("chargeMeta");
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound nbtTag) {
        nbtTag.setInteger("chargeMeta", chargeMeta);
    }

    public void onUpdate() {
        super.onUpdate();

        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        this.motionY -= 0.029999999329447746D;

        if (this.worldObj.getBlock(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posY), MathHelper.floor_double(this.posZ)).getMaterial() == Material.lava) {
            this.motionY = 0.20000000298023224D;
            this.motionX = (double) ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
            this.motionZ = (double) ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
        }

        this.func_145771_j(this.posX, (this.boundingBox.minY + this.boundingBox.maxY) / 2.0D, this.posZ);

        this.moveEntity(this.motionX, this.motionY, this.motionZ);
        float f = 0.98F;

        if (this.onGround) {
            f = this.worldObj.getBlock(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.boundingBox.minY) - 1, MathHelper.floor_double(this.posZ)).slipperiness * 0.98F;
        }

        this.motionX *= (double) f;
        this.motionY *= 0.9800000190734863D;
        this.motionZ *= (double) f;

        if (this.onGround) {
            this.motionY *= -0.8999999761581421D;
        }
    }

    public boolean handleWaterMovement() {
        return this.worldObj.handleMaterialAcceleration(this.boundingBox, Material.water, this);
    }

    public void onCollideWithPlayer(EntityPlayer entityPlayer) {
        if (!this.worldObj.isRemote) {
            if (MinecraftForge.EVENT_BUS.post(new PlayerPickupChargeEvent(entityPlayer, this.getChargeType()))) return;
            entityPlayer.onItemPickup(this, 1);
            this.setDead();
        }
    }

    public ChargeType getChargeType() {
        return ChargeType.values()[chargeMeta];
    }
}
