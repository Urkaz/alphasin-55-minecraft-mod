package urkaz.alphasin.core.entity.charges;

import net.minecraft.world.World;

public class EntityChargeGem extends EntityCharge {

    public EntityChargeGem(World world) {
        super(world);
        super.setChargeMeta(3);
    }

    public EntityChargeGem(World world, double x, double y, double z) {
        super(world, x, y, z, 3);
    }
}
