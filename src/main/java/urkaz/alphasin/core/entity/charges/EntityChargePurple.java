package urkaz.alphasin.core.entity.charges;

import net.minecraft.world.World;

public class EntityChargePurple extends EntityCharge {

    public EntityChargePurple(World world) {
        super(world);
        super.setChargeMeta(2);
    }

    public EntityChargePurple(World world, double x, double y, double z) {
        super(world, x, y, z, 2);
    }
}
