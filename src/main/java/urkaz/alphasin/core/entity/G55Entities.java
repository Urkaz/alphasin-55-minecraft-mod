package urkaz.alphasin.core.entity;

import cpw.mods.fml.common.registry.EntityRegistry;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.entity.charges.*;

public class G55Entities {

    public static void registerEntities() {
        registerEntity(EntityCharge.class, "entityCharge");
        registerEntity(EntityChargeCitronox.class, "entityChargeCitronox");
        registerEntity(EntityChargeEterium.class, "entityChargeEterium");
        registerEntity(EntityChargePurple.class, "entityChargePurple");
        registerEntity(EntityChargeGem.class, "entityChargeGem");
    }

    public static void registerEntity(Class entityClass, String name) {
        int entityID = EntityRegistry.findGlobalUniqueEntityId();

        EntityRegistry.registerGlobalEntityID(entityClass, name, entityID);
        EntityRegistry.registerModEntity(entityClass, name, entityID, Galaxy55Core.instance, 64, 1, true);
    }
}
