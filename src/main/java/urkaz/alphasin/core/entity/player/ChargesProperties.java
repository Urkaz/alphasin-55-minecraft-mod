package urkaz.alphasin.core.entity.player;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import urkaz.alphasin.core.Galaxy55Core;
import urkaz.alphasin.core.network.ChargesPacket;
import urkaz.alphasin.core.util.ChargeType;
import urkaz.alphasin.core.util.References;

public class ChargesProperties implements IExtendedEntityProperties {

    public static String identifier = References.MODID + "_preferences";

    private final EntityPlayer player;

    private int[] charges;

    public ChargesProperties(EntityPlayer player) {
        this.player = player;
        this.charges = new int[4];
    }

    public static void register(EntityPlayer player) {
        player.registerExtendedProperties(ChargesProperties.identifier, new ChargesProperties(player));
    }

    public static ChargesProperties get(EntityPlayer player) {
        return (ChargesProperties) player.getExtendedProperties(identifier);
    }

    @Override
    public void saveNBTData(NBTTagCompound compound) {
        compound.setInteger("citronox", charges[0]);
        compound.setInteger("eterium", charges[1]);
        compound.setInteger("purple", charges[2]);
        compound.setInteger("gems", charges[3]);
    }

    @Override
    public void loadNBTData(NBTTagCompound compound) {
        charges[0] = compound.getInteger("citronox");
        charges[1] = compound.getInteger("eterium");
        charges[2] = compound.getInteger("purple");
        charges[3] = compound.getInteger("gems");
    }

    @Override
    public void init(Entity entity, World world) {
    }

    public final void sync() {
        if (!player.worldObj.isRemote) {
            EntityPlayerMP toPlayer = (EntityPlayerMP) player;
            Galaxy55Core.network.sendTo(new ChargesPacket(charges), toPlayer);
        }
    }

    public boolean consumeCharge(ChargeType chargeType) {
        if (--charges[chargeType.ordinal()] > 0)
            charges[chargeType.ordinal()] = 0;
        this.sync();
        return true;
    }

    public boolean addCharge(ChargeType chargeType, int amount) {
        charges[chargeType.ordinal()] += amount;
        this.sync();
        return true;
    }

    public void setCharges(int[] charges) {
        this.charges = charges;
        this.sync();
    }

    public int[] getAllCharges() {
        return charges;
    }

    public int getCharge(ChargeType chargeType) {
        return charges[chargeType.ordinal()];
    }
}
