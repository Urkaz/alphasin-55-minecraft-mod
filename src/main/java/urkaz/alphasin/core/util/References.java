package urkaz.alphasin.core.util;

public class References {
    /*			Mod Definations			*/
    public final static String MODID = "alphasinaddon";
    public final static String MODNAME = "AlphaSin Addon for Galacticraft";

    public static final int LOCALMAJVERSION = 0;
    public static final int LOCALMINVERSION = 2;

    public final static String MODVER = LOCALMAJVERSION + "." + LOCALMINVERSION;

    /*			Proxy Names				*/
    public static final String ClientProxy_Location = "urkaz.alphasin.core.proxy.ClientProxy";
    public static final String CommonProxy_Location = "urkaz.alphasin.core.proxy.CommonProxy";

    /*			Texture Prefixs			*/
    public static final String Assets_Prefix = "alphasinaddon";
    public static final String Texture_Prefix = Assets_Prefix + ":";
}
