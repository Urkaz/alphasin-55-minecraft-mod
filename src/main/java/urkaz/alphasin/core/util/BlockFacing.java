package urkaz.alphasin.core.util;

public enum BlockFacing {
    DOWN(0f, -1f, 0f), UP(0f, 1f, 0f), NORTH(0f, 0f, -1f), SOUTH(0f, 0f, 1f), WEST(-1f, 0f, 0f), EAST(1f, 0f, 0f);

    public float facingX;
    public float facingY;
    public float facingZ;

    BlockFacing(float x, float y, float z) {
        this.facingX = x;
        this.facingY = y;
        this.facingZ = z;
    }

    public static BlockFacing getFromSide(int side) {
        return BlockFacing.values()[side];
    }
}
