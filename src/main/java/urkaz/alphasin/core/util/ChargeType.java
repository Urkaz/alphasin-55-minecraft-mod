package urkaz.alphasin.core.util;

public enum ChargeType {
    CITRONOX("citronoxCharge"), ETERIUM("eteriumCharge"), PURPLE("purpleCharge"), GEM("greenGem");

    private final String name;

    ChargeType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
