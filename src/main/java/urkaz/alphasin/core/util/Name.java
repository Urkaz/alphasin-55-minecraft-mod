package urkaz.alphasin.core.util;

public class Name {

    //Creative tab
    public static final String creativeTab = "g55tab";

    //Blocks
    public static final String groupDirt_name = "g55dirt";
    public static final String groupGrass_name = "g55grass";
    public static final String groupRock_name = "g55rock";
    public static final String groupMineral_name = "g55mineral";
    public static final String groupMineralItem_name = "g55mineralitem";
    public static final String groupLeaves_name = "g55leaves";
    public static final String groupWood_name = "g55wood";
    public static final String groupAggregate_name = "g55aggregate";
    public static final String groupStain_name = "g55stain";
    public static final String blockWoodPlank_name = "g55woodPlank";

    public static final String blockBlindage_name = "g55blindage";
    public static final String blockCraftingPanel_name = "g55craftingPanel";
    public static final String blockSynthesizer_name = "g55synthesizer";

    public static final String blockMinAluminium_name = "aluminium";
    public static final String blockMinIron_name = "iron";

    public static final String blockCitronox_name = "g55citronox";
    public static final String blockCitronoxOld_name = "g55oldCitronox";

    public static final String groupCharges_name = "g55charges";

    public static final String blockFakeBlock_name = "g55fakeBlock";

    public static final String itemMiningLaser_name = "g55miningLaser";

    //Meta Blocks
    public static final String[] mBlockDirt_subs = new String[]{"redDirt", "purpleDirt", "glacialDirt", "acidDirt"};
    public static final String[] mBlockGrass_subs = new String[]{"redGrass", "purpleGrass", "glacialGrass", "acidGrass"};
    public static final String[] mBlockRock_subs = new String[]{"rock", "glacialRock", "acidRock", "desertRock"};
    public static final String[] mBlockMineral_subs = new String[]{"aluminium", "iron"};
    public static final String[] mBlockLeaves_subs = new String[]{"leaves", "pinkLeaves", "purpleLeaves", "brownLeaves", "alienLeaves", "alienSpacialLeaves", "frondosity", "frondositySpace", "pineLeaves", "pineLeavesSnow"};
    public static final String[] mBlockWood_subs = new String[]{"redWood", "pineWood", "grozarbWood", "grozarbWood_empty", "grozarbEyeWood"};
    public static final String[] mBlockAggregate_subs = new String[]{"aggregateUncolored", "aggregateRed", "aggregatePurple", "aggregateWhite", "aggregateGreen", "aggregateYellow"};
    public static final String[] mBlockStain_subs = new String[]{"stainDead", "stainRed", "stainPurple", "stainWhite", "stainGreen", "stainYellow"};
    public static final String[] mItemCharges_subs = new String[]{"citronoxCharge", "eteriumCharge", "purpleCharge", "greenGem"};
}