# Galaxy55 Addon for Galacticraft
<p align="center">
<img src="https://raw.githubusercontent.com/Urkaz/Galaxy55Galacticraft/master/src/main/resources/assets/galaxy55addon/textures/gui/logo.png" alt="Galaxy55 Addon for Galacticraft">
<br>
	<a href="https://travis-ci.org/Urkaz/Galaxy55Galacticraft">
		<img src="https://img.shields.io/travis/Urkaz/Galaxy55Galacticraft.svg" alt="Build Status">
	</a>
	<br>
		<img src="https://img.shields.io/badge/minecraft-1.7.10-blue.svg" alt="Minecraft version">
	<a href="http://files.minecraftforge.net/">
		<img src="https://img.shields.io/badge/forge-10.13.4.1492-blue.svg" alt="Forge version">
	</a>
	<a href="http://micdoodle8.com/mods/galacticraft">
		<img src="https://img.shields.io/badge/galacticraft-3.0.12.167-blue.svg" alt="Galacicraft version">
	</a>
	<br>
	<strong><a href="#">Website</a> | <a href="#">Minecraft Forum Thread</a> | <a href="#">Galacticraft Forum Thread</a></strong>
</p>
